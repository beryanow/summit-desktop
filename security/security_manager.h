#ifndef SECURITYMANAGER_H
#define SECURITYMANAGER_H

#include "utilities/common/network/request.h"
#include "utilities/common/nonstd/optional.h"

#include <QString>
#include <stdexcept>

namespace summit {

class SecurityException : public std::exception {
  const std::string message;

public:
  explicit SecurityException(const std::string &message);

  const char *what() const override;
};

class SecurityManager {
public:
  using AuthorizationToken = QString;

  static SecurityManager &getInstance();

  bool hasToken() const;
  const AuthorizationToken &getToken();

  void setToken(const AuthorizationToken &token);

private:
  nonstd::optional<AuthorizationToken> token;

  SecurityManager();
};
} // namespace summit

#endif // SECURITYMANAGER_H
