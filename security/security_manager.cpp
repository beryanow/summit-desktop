#include "security_manager.h"

namespace summit {

SecurityException::SecurityException(const std::string &message)
    : message(message) {}

const char *SecurityException::what() const { return message.c_str(); }

SecurityManager &SecurityManager::getInstance() {
  static SecurityManager instance;
  return instance;
}

bool SecurityManager::hasToken() const { return token; }

const SecurityManager::AuthorizationToken &SecurityManager::getToken() {
  if (!token) {
    throw SecurityException("Token not found");
  }
  return token.value();
}

void SecurityManager::setToken(
    const SecurityManager::AuthorizationToken &token) {
  this->token = token;
}

SecurityManager::SecurityManager() {}

} // namespace summit
