#ifndef IMAGESREPOSITORY_H
#define IMAGESREPOSITORY_H

#include "data/common/source/remote/default_data_loader.h"
#include "data/common/source/remote/default_data_updater.h"
#include "images_data_source.h"

#include <mutex>
#include <unordered_map>
#include <utilities/common/nonstd/optional.h>

namespace summit {

class ImagesCache {
  std::unordered_map<int, ImagePointer> images_cache;

  mutable std::mutex mutex;

public:
  void addImage(int id, const ImagePointer &image);

  nonstd::optional<ImagePointer> getImage(int id) const;
};

class ImagesRepository : public ImagesDataSource {

  static const QString IMAGES_API;
  static ImagesCache cache;

  DefaultDataLoader loader;
  DefaultDataUpdater updater;

public:
  static std::unique_ptr<ImagesRepository> create();

  void sendImage(const ImagePointer &image) override;
  void loadImage(int id) override;

private:
  ImagesRepository();
};

} // namespace summit

#endif // IMAGESREPOSITORY_H
