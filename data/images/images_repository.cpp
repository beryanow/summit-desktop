#include "images_repository.h"

namespace summit {

const QString ImagesRepository::IMAGES_API = "api/image";
ImagesCache ImagesRepository::cache{};

ImagesRepository::ImagesRepository() {}

std::unique_ptr<ImagesRepository> ImagesRepository::create() {
  std::unique_ptr<ImagesRepository> INSTANCE(new ImagesRepository);
  return INSTANCE;
}

void ImagesRepository::sendImage(const ImagePointer &image) {
  /* Send image */
  updater.updateObject<ImageJsonAdapter, ImageJsonAdapter>(
      IMAGES_API,
      [this](const ImageJsonAdapter &adapter) {
        emit sendSuccess(ImagePointer::create(adapter));
      },
      [this](const DataError &error) {
        /* On failure */
        emit sendFailure(error);
      },
      image->makeAdapter(), network::RequestType::POST);
}

void ImagesRepository::loadImage(int id) {
  /* Lookup cache */
  auto image_wrapper = cache.getImage(id);
  if (image_wrapper) {
    emit loadSuccess(image_wrapper.value());
    return;
  }

  QVariantMap map({{"id", id}});

  loader.loadSingleObject<ImageJsonAdapter>(
      IMAGES_API,
      [this, id](const ImageJsonAdapter &adapter) {
        /* On success */
        auto image = ImagePointer::create(adapter);
        cache.addImage(id, image);

        emit loadSuccess(image);
      },
      [this](const DataError &error) {
        /* On failure */
        emit loadFailure(error);
      },
      map);
}

void ImagesCache::addImage(int id, const ImagePointer &image) {
  std::lock_guard<std::mutex> lock(mutex);
  images_cache[id] = image;
}

nonstd::optional<ImagePointer> ImagesCache::getImage(int id) const {
  std::lock_guard<std::mutex> lock(mutex);
  auto it = images_cache.find(id);
  if (it == images_cache.end()) {
    return nonstd::nullopt;
  }
  return it->second;
}

} // namespace summit
