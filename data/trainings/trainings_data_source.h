#ifndef TRAININGS_DATA_SOURCE_H
#define TRAININGS_DATA_SOURCE_H

#include <QObject>

#include "data/common/data_error.h"
#include "model/trainings/training.h"
#include "model/trainings/training_type.h"

namespace summit {

class TrainingsDataSource : public QObject {
  Q_OBJECT

public:
  virtual void loadDefaultTraining(const TrainingType &type) = 0;
  virtual void loadAllTrainings() = 0;
  virtual void updateTraining(const Training &training) = 0;
  virtual void deleteTraining(int id) = 0;

  virtual ~TrainingsDataSource() = default;
signals:
  void onDefaultTrainingLoadSuccess(const Training &training);
  void onTrainingListLoadSuccess(const TrainingList &trainings);
  void onTrainingUpdateSuccess();
  void onTrainingDeleteSuccess();

  void onFailure(const DataError &error);
};

} // namespace summit

#endif // TRAININGS_DATA_SOURCE_H
