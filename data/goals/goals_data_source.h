#ifndef GOALS_DATA_SOURCE_H
#define GOALS_DATA_SOURCE_H

#include <list>

#include "data/common/data_error.h"
#include "model/goals/goal.h"

namespace summit {

class GoalsDataSource : public QObject {
  Q_OBJECT

public:
  virtual ~GoalsDataSource() = default;

  virtual void loadGoals() = 0;
  virtual void updateGoal(const Goal &goal) = 0;
  virtual void deleteGoal(int id) = 0;

signals:
  void loadSuccess(const QList<Goal> &goals);
  void loadFailure(const DataError &error);

  void updateSuccess();
  void updateFailure(const DataError &error);

  void deleteSuccess();
  void deleteFailure(const DataError &error);
};

} // namespace summit
#endif // GOALS_DATA_SOURCE_H
