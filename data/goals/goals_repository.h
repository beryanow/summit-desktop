#ifndef GOALSREPOSITORY_H
#define GOALSREPOSITORY_H

#include "data/common/source/remote/default_data_deleter.h"
#include "data/common/source/remote/default_data_loader.h"
#include "data/common/source/remote/default_data_updater.h"
#include "goals_data_source.h"

namespace summit {

class GoalsRepository : public GoalsDataSource {

  static const QString LOAD_GOALS_API;
  static const QString UPDATE_GOAL_API;
  static const QString DELETE_GOAL_API;

  DefaultDataLoader loader;
  DefaultDataUpdater updater;
  DefaultDataDeleter deleter;

public:
  static std::unique_ptr<GoalsRepository> create();

  void loadGoals() override;
  void updateGoal(const Goal &goal) override;
  void deleteGoal(int id) override;

private:
  GoalsRepository();
};

} // namespace summit

#endif // GOALSREPOSITORY_H
