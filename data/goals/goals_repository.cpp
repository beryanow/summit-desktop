#include "goals_repository.h"

namespace summit {

const QString GoalsRepository::LOAD_GOALS_API = "api/goal/getAll";
const QString GoalsRepository::UPDATE_GOAL_API = "api/goal";
const QString GoalsRepository::DELETE_GOAL_API = "api/goal/delete";

GoalsRepository::GoalsRepository() {}

std::unique_ptr<GoalsRepository> GoalsRepository::create() {
  std::unique_ptr<GoalsRepository> INSTANCE(new GoalsRepository);
  return INSTANCE;
}

void GoalsRepository::loadGoals() {
  loader.loadAllObjects<Goal>(
      LOAD_GOALS_API,
      [this](const GoalsList &goals) { emit loadSuccess(goals); },
      [this](const DataError &error) { emit loadFailure(error); });
}

void GoalsRepository::updateGoal(const Goal &goal) {
  updater.updateObject<Goal, void>(
      UPDATE_GOAL_API, [this]() { emit updateSuccess(); },
      [this](const DataError &error) { emit updateFailure(error); }, goal);
}

void GoalsRepository::deleteGoal(int id) {
  deleter.deteteObject(
      DELETE_GOAL_API, [this]() { emit deleteSuccess(); },
      [this](const DataError &error) { emit deleteFailure(error); }, id);
}

} // namespace summit
