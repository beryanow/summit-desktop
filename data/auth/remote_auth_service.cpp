#include "remote_auth_service.h"

#include "constants/network/network_constants.h"
#include "utilities/common/json/json_parser.h"
#include "utilities/common/network/request.h"

namespace summit {

const QString RemoteAuthService::LOGIN_API_NAME = "login";
const QString RemoteAuthService::REGISTER_API_NAME = "auth/register";

std::shared_ptr<RemoteAuthService> &RemoteAuthService::getInstance() {
  static std::shared_ptr<RemoteAuthService> INSTANCE(new RemoteAuthService);
  return INSTANCE;
}

void RemoteAuthService::signIn(const AuthenticationData &auth_data) {
  using namespace network;
  using namespace json;

  Requester *requester = new Requester(this);
  requester->initialize(REMOTE_HOSTNAME, REMOTE_PORT, nullptr);

  connect(requester, &Requester::tokenAccepted, this,
          [this](const SecurityManager::AuthorizationToken &token) {
            emit signInSuccess(token);
          });

  connect(requester, &Requester::requestFailure, this,
          [this](const QJsonDocument &doc, const QString &error) {
            QJsonObject json = doc.object();
            emit signInFailure(
                DataError{json["code"].toInt(), json["message"].toString()});
          });

  /* On finish cleanup */
  connect(requester, &Requester::finished, requester, &Requester::deleteLater);

  requester->sendRequest(
      LOGIN_API_NAME, RequestType::POST,
      QJsonDocument{JsonParser<AuthenticationData>::toJson(auth_data)});
}

void RemoteAuthService::signUp(const RegisterData &register_data) {
  using namespace network;
  using namespace json;

  Requester *requester = new Requester(this);
  requester->initialize(REMOTE_HOSTNAME, REMOTE_PORT, nullptr);

  connect(requester, &Requester::requestSuccess, this,
          [this](const QJsonDocument &) { emit signUpSuccess(); });

  connect(requester, &Requester::requestFailure, this,
          [this](const QJsonDocument &doc, const QString &error) {
            QJsonObject json = doc.object();
            emit signUpFailure(
                DataError{json["code"].toInt(), json["message"].toString()});
          });

  /* On finish cleanup */
  connect(requester, &Requester::finished, requester, &Requester::deleteLater);

  requester->sendRequest(
      REGISTER_API_NAME, RequestType::POST,
      QJsonDocument{JsonParser<RegisterData>::toJson(register_data)});
}

RemoteAuthService::RemoteAuthService() {}

} // namespace summit
