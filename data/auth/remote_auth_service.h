#ifndef REMOTESIGNINSERVICE_H
#define REMOTESIGNINSERVICE_H

#include "auth_service.h"

namespace summit {

class RemoteAuthService : public AuthService {
  static const QString LOGIN_API_NAME;
  static const QString REGISTER_API_NAME;

public:
  static std::shared_ptr<RemoteAuthService> &getInstance();

  void signIn(const AuthenticationData &auth_data) override;
  void signUp(const RegisterData &register_data) override;

private:
  RemoteAuthService();
};

} // namespace summit

#endif // REMOTESIGNINSERVICE_H
