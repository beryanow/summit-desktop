#include "nutrition_repository.h"

namespace summit {

const QString NutritionRepository::LOAD_RECIPES_API = "api/nutrition/getAll";
const QString NutritionRepository::UPDATE_RECIPE_API = "api/nutrition";
const QString NutritionRepository::DELETE_RECIPE_API = "api/nutrition/delete";

NutritionRepository::NutritionRepository() {}

std::unique_ptr<NutritionRepository> NutritionRepository::create() {
  std::unique_ptr<NutritionRepository> INSTANCE(new NutritionRepository);
  return INSTANCE;
}

void NutritionRepository::loadRecipes() {
  loader.loadAllObjects<Recipe>(
      LOAD_RECIPES_API,
      [this](const RecipeList &recipes) { emit loadSuccess(recipes); },
      [this](const DataError &error) { emit loadFailure(error); });
}

void NutritionRepository::updateRecipe(const Recipe &recipe) {
  updater.updateObject<Recipe, void>(
      UPDATE_RECIPE_API, [this]() { emit updateSuccess(); },
      [this](const DataError &error) { emit updateFailure(error); }, recipe);
}

void NutritionRepository::deleteRecipe(int id) {
  deleter.deteteObject(
      DELETE_RECIPE_API, [this]() { emit deleteSuccess(); },
      [this](const DataError &error) { emit deleteFailure(error); }, id);
}

} // namespace summit
