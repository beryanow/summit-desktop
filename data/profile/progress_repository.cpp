#include "progress_repository.h"

namespace summit {

const QString ProgressRepository::LOAD_PROGRESS_API = "api/profile/progress";

std::unique_ptr<ProgressRepository> ProgressRepository::create() {
  std::unique_ptr<ProgressRepository> INSTANCE(new ProgressRepository);
  return INSTANCE;
}

void ProgressRepository::loadProgress(const QDate &date) {
  QVariantMap map;
  if (!date.isNull()) {
    map["date"] = date.toString(Qt::ISODate);
  }

  loader.loadAllObjects<UserProgress>(
      LOAD_PROGRESS_API,
      [this](const UserProgressList &progress) {
        qDebug() << "OK";
        emit loadSuccess(progress);
      },
      [this](const DataError &error) { emit loadFailure(error); });
}

ProgressRepository::ProgressRepository() {}

} // namespace summit
