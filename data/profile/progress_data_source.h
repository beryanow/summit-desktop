#ifndef PROGRESS_DATA_SOURCE_H
#define PROGRESS_DATA_SOURCE_H

#include <QObject>

#include "data/common/data_error.h"
#include "model/profile/user_progress.h"

namespace summit {

class ProgressDataSource : public QObject {
  Q_OBJECT

public:
  virtual ~ProgressDataSource() = default;

  virtual void loadProgress(const QDate &date) = 0;

signals:
  void loadSuccess(const UserProgressList &progress);
  void loadFailure(const DataError &error);
};

} // namespace summit

#endif // PROGRESS_DATA_SOURCE_H
