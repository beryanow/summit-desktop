#ifndef PROGRESS_REPOSITORY_H
#define PROGRESS_REPOSITORY_H

#include "data/common/source/remote/default_data_loader.h"
#include "progress_data_source.h"

namespace summit {

class ProgressRepository : public ProgressDataSource {

  static const QString LOAD_PROGRESS_API;

  DefaultDataLoader loader;

public:
  static std::unique_ptr<ProgressRepository> create();

  void loadProgress(const QDate &date) override;

private:
  ProgressRepository();
};

} // namespace summit

#endif // PROGRESS_REPOSITORY_H
