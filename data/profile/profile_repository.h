#ifndef PROFILEREPOSITORY_H
#define PROFILEREPOSITORY_H

#include <QObject>

#include "data/common/source/remote/default_data_loader.h"
#include "data/common/source/remote/default_data_updater.h"
#include "profile_data_source.h"

namespace summit {

class ProfileRepository : public ProfileDataSource {

  static const QString PROFILE_API;

  DefaultDataLoader loader;
  DefaultDataUpdater updater;

public:
  static std::unique_ptr<ProfileRepository> create();

  void loadProfile() override;
  void updateProfile(const UserProfilePointer &profile) override;

private:
  ProfileRepository();
};

} // namespace summit

#endif // PROFILEREPOSITORY_H
