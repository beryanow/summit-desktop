#ifndef SETUP_H
#define SETUP_H

#include "constants/network/network_constants.h"
#include "utilities/common/network/request.h"

namespace summit {

#define SETUP_SUCCESS 0
#define TOKEN_NOT_FOUND 1

int setupRequester(network::Requester *requester);
} // namespace summit

#endif // SETUP_H
