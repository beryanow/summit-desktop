#include "setup.h"

namespace summit {

int setupRequester(network::Requester *requester) {
  using namespace network;

  requester->initialize(REMOTE_HOSTNAME, REMOTE_PORT, nullptr);

  auto &security_manager = SecurityManager::getInstance();
  if (!security_manager.hasToken()) {
    requester->deleteLater();
    return TOKEN_NOT_FOUND;
  }
  requester->setToken(security_manager.getToken());
  return SETUP_SUCCESS;
}

} // namespace summit
