#ifndef DEFAULT_DATA_UPDATER_H
#define DEFAULT_DATA_UPDATER_H

#include "constants/network/network_constants.h"
#include "data/common/data_error.h"
#include "security/security_manager.h"
#include "setup.h"

#include "utilities/common/json/json_parser.h"
#include "utilities/common/network/request.h"
#include "utilities/common/nonstd/error.h"

namespace summit {

static const QString UPDATER_DATA_ERROR_MESSAGE =
    QString::fromUtf8("Сервер отправил недействительные данные");

class DefaultDataUpdater : public QObject {
public:
  DefaultDataUpdater() = default;

  template <class RequestDataType, class ResponceDataType, class SuccessFuction,
            class FailureFunction>
  void
  updateObject(const QString &api, SuccessFuction on_success,
               FailureFunction on_failure, const RequestDataType &object,
               network::RequestType request_type = network::RequestType::PUT);

  template <class DataType, class SuccessFunction, class FailureFunction>
  struct SuccessResponceHandler {
    static void handle(SuccessFunction on_success, FailureFunction on_failure,
                       const QJsonDocument &json) {
      if (!json.isObject()) {
        on_failure(DataError{-1, UPDATER_DATA_ERROR_MESSAGE});
      }
      try {
        on_success(json::JsonParser<DataType>::fromJson(json.object()));
      } catch (const json::JsonParseException &e) {
        on_failure(DataError{-1, UPDATER_DATA_ERROR_MESSAGE});
      }
    }
  };

  template <class SuccessFunction, class FailureFunction>
  struct SuccessResponceHandler<void, SuccessFunction, FailureFunction> {
    static void handle(SuccessFunction on_success, FailureFunction,
                       const QJsonDocument &) {
      on_success();
    }
  };
};

template <class RequestDataType, class ResponceDataType, class SuccessFuction,
          class FailureFunction>
void DefaultDataUpdater::updateObject(const QString &api,
                                      SuccessFuction on_success,
                                      FailureFunction on_failure,
                                      const RequestDataType &object,
                                      network::RequestType request_type) {
  using namespace network;

  Requester *requester = new Requester(this);
  auto result = setupRequester(requester);
  if (result != SETUP_SUCCESS) {
    emit on_failure(DataError{});
    return;
  }

  /* Connect */
  connect(
      requester, &Requester::requestSuccess, this,
      [this, on_success, on_failure](const QJsonDocument &responce) {
        SuccessResponceHandler<ResponceDataType, SuccessFuction,
                               FailureFunction>::handle(std::move(on_success),
                                                        std::move(on_failure),
                                                        responce);
      });

  connect(
      requester, &Requester::requestFailure, this,
      [this, on_failure](const QJsonDocument &response, const QString &error) {
        QJsonObject json = response.object();
        on_failure(DataError{json["code"].toInt(), json["message"].toString()});
      });

  /* On finish cleanup */
  connect(requester, &Requester::finished, requester, &Requester::deleteLater);
  requester->sendRequest(
      api, request_type,
      QJsonDocument{json::JsonParser<RequestDataType>::toJson(object)});
}

} // namespace summit

#endif // DEFAULT_DATA_UPDATER_H
