#ifndef DEFAULTDATADELETER_H
#define DEFAULTDATADELETER_H

#include "constants/network/network_constants.h"
#include "data/common/data_error.h"
#include "security/security_manager.h"
#include "setup.h"

#include "utilities/common/json/json_parser.h"
#include "utilities/common/network/request.h"
#include "utilities/common/nonstd/error.h"

namespace summit {

class DefaultDataDeleter : public QObject {
  Q_OBJECT
public:
  template <class SuccessFuction, class FailureFunction>
  void deteteObject(const QString &api, SuccessFuction on_success,
                    FailureFunction on_failure, int id);
};

template <class SuccessFuction, class FailureFunction>
void DefaultDataDeleter::deteteObject(const QString &api,
                                      SuccessFuction on_success,
                                      FailureFunction on_failure, int id) {
  using namespace network;

  Requester *requester = new Requester(this);
  auto result = setupRequester(requester);
  if (result != SETUP_SUCCESS) {
    emit on_failure(DataError{});
    return;
  }

  /* Connect */
  connect(
      requester, &Requester::requestSuccess, this,
      [this, on_success, on_failure](const QJsonDocument &d) { on_success(); });

  connect(
      requester, &Requester::requestFailure, this,
      [this, on_failure](const QJsonDocument &response, const QString &error) {
        QJsonObject json = response.object();
        on_failure(DataError{json["code"].toInt(), json["message"].toString()});
      });

  /* On finish cleanup */
  connect(requester, &Requester::finished, requester, &Requester::deleteLater);

  const QString delete_api = api + '/' + QString::number(id);
  requester->sendRequest(delete_api, RequestType::DELETE);
}

} // namespace summit

#endif // DEFAULTDATADELETER_H
