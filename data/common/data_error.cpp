#include "data_error.h"

#include <QDebug>

DataError::DataError(const int code, const QString &message)
    : code(code), message(message) {
  qDebug() << message;
}
