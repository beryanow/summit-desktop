#ifndef DATAERROR_H
#define DATAERROR_H

#include <QString>

class DataError {
  int code;
  QString message;

public:
  DataError() = default;

  DataError(const int code, const QString &message);

  int getCode() const { return code; }
  const QString &getMessage() const { return message; }
};

#endif // DATAERROR_H
