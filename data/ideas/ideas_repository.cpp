#include "ideas_repository.h"

namespace summit {

const QString IdeasRepository::LOAD_IDEAS_API = "api/idea/getAll";
const QString IdeasRepository::UPDATE_IDEA_API = "api/idea";
const QString IdeasRepository::DELETE_IDEA_API = "api/ideas/delete";

std::unique_ptr<IdeasRepository> IdeasRepository::create() {
  std::unique_ptr<IdeasRepository> INSTANCE(new IdeasRepository);
  return INSTANCE;
}

void IdeasRepository::loadIdeas() {
  loader.loadAllObjects<Idea>(
      LOAD_IDEAS_API,
      [this](const IdeasList &ideas) { emit loadSuccess(ideas); },
      [this](const DataError &error) { emit loadFailure(error); });
}

void IdeasRepository::updateIdea(const Idea &idea) {
  updater.updateObject<Idea, void>(
      UPDATE_IDEA_API, [this]() { emit updateSuccess(); },
      [this](const DataError &error) { emit updateFailure(error); }, idea);
}

void IdeasRepository::deleteIdea(int id) {
  deleter.deteteObject(
      DELETE_IDEA_API, [this]() { emit deleteSuccess(); },
      [this](const DataError &error) { emit deleteFailure(error); }, id);
}

IdeasRepository::IdeasRepository() {}

} // namespace summit
