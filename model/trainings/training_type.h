#ifndef TRAINING_TYPE_H
#define TRAINING_TYPE_H

#include <QString>

namespace summit {

class TrainingType {
public:
  enum Type { GYM = 0, STREET = 1 };

  TrainingType(Type type) : type(type) {}

  QString toString() const {
    switch (type) {
    case Type::GYM:
      return "зал";
    case Type::STREET:
      return "улица";
    }
    return {};
  }

private:
  Type type;
};

} // namespace summit

#endif // TRAINING_TYPE_H
