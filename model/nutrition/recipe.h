#ifndef RECIPE_H
#define RECIPE_H

#include "utilities/common/json/json_parser.h"

namespace summit {

class Ingredient {
  JSON_META_ACCESS(Ingredient)

  JSON_ATTRIBUTE(json::types::Integer, id)
  JSON_ATTRIBUTE(json::types::String, name)
  JSON_ATTRIBUTE(json::types::String, amount)

public:
  Ingredient();
};

class Recipe {
  JSON_META_ACCESS(Recipe)

  JSON_ATTRIBUTE(json::types::Integer, id)
  JSON_ATTRIBUTE(json::types::String, name)
  JSON_ATTRIBUTE(json::types::String, recipe)
  JSON_ATTRIBUTE(json::types::String, description)
  JSON_ATTRIBUTE(json::types::Vector<Ingredient>, ingredients)
  JSON_ATTRIBUTE(json::types::String, size);

  JSON_ATTRIBUTE(json::types::Integer, backgroundId)
public:
  Recipe();
};

using RecipeList = QList<Recipe>;

} // namespace summit

ADD_JSON_PROPERTIES(summit::Ingredient,
                    /* Properties */
                    JSON_PROPERTY(summit::Ingredient, id),
                    JSON_PROPERTY(summit::Ingredient, name),
                    JSON_PROPERTY(summit::Ingredient, amount))

ADD_JSON_PROPERTIES(summit::Recipe,
                    /* Properties */
                    JSON_PROPERTY(summit::Recipe, id),
                    JSON_PROPERTY(summit::Recipe, name),
                    JSON_PROPERTY(summit::Recipe, recipe),
                    JSON_PROPERTY(summit::Recipe, description),
                    JSON_PROPERTY(summit::Recipe, ingredients),
                    JSON_PROPERTY(summit::Recipe, size),
                    JSON_PROPERTY(summit::Recipe, backgroundId))

#endif // RECIPE_H
