#ifndef IDEA_H
#define IDEA_H

#include "utilities/common/json/json_parser.h"

namespace summit {

class Idea {
  JSON_META_ACCESS(Idea)

  JSON_ATTRIBUTE(json::types::Integer, id)
  JSON_ATTRIBUTE(json::types::String, content)
  JSON_ATTRIBUTE(json::types::String, name);
  JSON_ATTRIBUTE(json::types::String, size);

  JSON_ATTRIBUTE(json::types::Integer, backgroundId)
public:
  Idea();
};

using IdeasList = QList<Idea>;

} // namespace summit

ADD_JSON_PROPERTIES(summit::Idea,
                    /* Properties */
                    JSON_PROPERTY(summit::Idea, id),
                    JSON_PROPERTY(summit::Idea, content),
                    JSON_PROPERTY(summit::Idea, name),
                    JSON_PROPERTY(summit::Idea, size),
                    JSON_PROPERTY(summit::Idea, backgroundId))

#endif // IDEA_H
