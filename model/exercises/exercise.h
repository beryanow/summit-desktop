#ifndef EXERCISE_H
#define EXERCISE_H

#include "utilities/common/json/json_parser.h"

namespace summit {

class Exercise {
  JSON_META_ACCESS(Exercise)

  JSON_ATTRIBUTE(json::types::Integer, id)
  JSON_ATTRIBUTE(json::types::String, name)
  JSON_ATTRIBUTE(json::types::String, description)
  JSON_ATTRIBUTE(json::types::String, type)
  JSON_ATTRIBUTE(json::types::Integer, trainingId)
  JSON_ATTRIBUTE(json::types::Integer, repetitions)

public:
  Exercise();
};

} // namespace summit

ADD_JSON_PROPERTIES(summit::Exercise,
                    /* Properties */
                    JSON_PROPERTY(summit::Exercise, id),
                    JSON_PROPERTY(summit::Exercise, name),
                    JSON_PROPERTY(summit::Exercise, description),
                    JSON_PROPERTY(summit::Exercise, type),
                    JSON_PROPERTY(summit::Exercise, trainingId),
                    JSON_PROPERTY(summit::Exercise, repetitions))

#endif // EXERCISE_H
