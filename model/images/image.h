#ifndef IMAGE_H
#define IMAGE_H

#include "utilities/common/json/json_parser.h"
#include "utilities/common/nonstd/optional.h"

#include <QPixmap>
#include <QSharedPointer>

namespace summit {

class ImageJsonAdapter {
  JSON_META_ACCESS(ImageJsonAdapter)

  JSON_ATTRIBUTE(json::types::Integer, id)
  JSON_ATTRIBUTE(json::types::String, content)
};

class ImageWrapper {
public:
  static constexpr int NO_IMAGE_ID = -1;

  ImageWrapper() = default;
  ImageWrapper(const ImageJsonAdapter &adapter);
  ImageWrapper(const QPixmap &pixmap);
  ImageWrapper(QPixmap &&pixmap);

  ImageWrapper(QByteArray binary);

  ImageWrapper
  getScaled(const QSize &size,
            Qt::AspectRatioMode aspect_mode = Qt::KeepAspectRatioByExpanding,
            Qt::TransformationMode mode = Qt::FastTransformation) const;

  ImageWrapper fit(const QSize &size) const;

  QPixmap &getAsPixmap() { return pixmap; }
  const QPixmap &getAsPixmap() const { return pixmap; }

  int getImageId() const;
  void setImageId(int id);

  QSize getSize() const { return pixmap.size(); }

  ImageJsonAdapter makeAdapter() const;

private:
  int id = NO_IMAGE_ID;
  QPixmap pixmap;
};

using ImagePointer = QSharedPointer<ImageWrapper>;

ImagePointer fromFile(const QString &filename);

void displayCentered(QPainter &painter, const ImageWrapper &image_wrapper);
void displayCentered(QPainter &painter, const QSize &size,
                     const ImageWrapper &image_wrapper);

} // namespace summit

ADD_JSON_PROPERTIES(summit::ImageJsonAdapter,
                    /* Properties */
                    JSON_PROPERTY(summit::ImageJsonAdapter, id),
                    JSON_PROPERTY(summit::ImageJsonAdapter, content))

#endif // IMAGE_H
