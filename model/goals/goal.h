#ifndef GOAL_H
#define GOAL_H

#include "model/tasks/task.h"
#include "utilities/common/json/json_parser.h"

namespace summit {

class Goal {
  JSON_META_ACCESS(Goal)

  JSON_ATTRIBUTE(json::types::Integer, id)
  JSON_ATTRIBUTE(json::types::String, name)
  JSON_ATTRIBUTE(json::types::String, description)
  JSON_ATTRIBUTE(json::types::String, size)
  JSON_ATTRIBUTE(json::types::String, statePlace)
  JSON_ATTRIBUTE(json::types::Vector<Task>, tasks);

  JSON_ATTRIBUTE(json::types::Integer, backgroundId)
public:
  Goal();
};

using GoalsList = QList<Goal>;
using GoalPointer = QSharedPointer<Goal>;

} // namespace summit

ADD_JSON_PROPERTIES(summit::Goal,
                    /* Properties: */
                    JSON_PROPERTY(summit::Goal, id),
                    JSON_PROPERTY(summit::Goal, name),
                    JSON_PROPERTY(summit::Goal, description),
                    JSON_PROPERTY(summit::Goal, size),
                    JSON_PROPERTY(summit::Goal, statePlace),
                    JSON_PROPERTY(summit::Goal, tasks),
                    JSON_PROPERTY(summit::Goal, backgroundId))

#endif // GOAL_H
