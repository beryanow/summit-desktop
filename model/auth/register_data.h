#ifndef REGISTERDATA_H
#define REGISTERDATA_H

#include "utilities/common/json/json_parser.h"

namespace summit {

class RegisterData {
  JSON_META_ACCESS(RegisterData)

  JSON_ATTRIBUTE(json::types::String, name)
  JSON_ATTRIBUTE(json::types::String, surname)
  JSON_ATTRIBUTE(json::types::Integer, age)

  JSON_ATTRIBUTE(json::types::String, username)
  JSON_ATTRIBUTE(json::types::String, email)
  JSON_ATTRIBUTE(json::types::String, password)

public:
  RegisterData();
};

} // namespace summit

ADD_JSON_PROPERTIES(summit::RegisterData,
                    /* Properties */
                    JSON_PROPERTY(summit::RegisterData, name),
                    JSON_PROPERTY(summit::RegisterData, surname),
                    JSON_PROPERTY(summit::RegisterData, age),
                    JSON_PROPERTY(summit::RegisterData, username),
                    JSON_PROPERTY(summit::RegisterData, email),
                    JSON_PROPERTY(summit::RegisterData, password))

#endif // REGISTERDATA_H
