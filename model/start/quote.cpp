#include "quote.h"

namespace summit {

Quote::Quote(const ImagePointer &image, const QString &text)
    : image(image), text(text) {}

void Quote::setImage(const ImagePointer &image) { this->image = image; }

const ImagePointer &Quote::getImage() const { return image; }

void Quote::setText(const QString &text) { this->text = text; }

void Quote::setText(QString &&text) { this->text = std::move(text); }

const QString &Quote::getText() const { return text; }

} // namespace summit
