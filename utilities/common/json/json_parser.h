#ifndef JSON_PARSER_H
#define JSON_PARSER_H

#include "properties.h"
#include "utilities/common/nonstd/emplace.h"

#include <algorithm>
#include <string>
#include <type_traits>

/* Add macro definitions for users */

#define JSON_ATTRIBUTE(TYPE, NAME)                                             \
private:                                                                       \
  nonstd::optional<TYPE> NAME = {};                                            \
                                                                               \
public:                                                                        \
  const TYPE &get_##NAME() const { return *NAME; }                             \
  TYPE &get_##NAME() { return *NAME; }                                         \
  bool has_##NAME() const { return NAME; }                                     \
                                                                               \
  template <class ARG_TYPE,                                                    \
            class F = typename std::enable_if<                                 \
                std::is_assignable<TYPE &, ARG_TYPE>::value>::type>            \
  void set_##NAME(ARG_TYPE &&NAME##_) {                                        \
    NAME = std::forward<ARG_TYPE>(NAME##_);                                    \
  }                                                                            \
                                                                               \
  void set_##NAME(nonstd::nullopt_t) { NAME = nonstd::nullopt; }

#define JSON_META_ACCESS(Class) friend json::Properties<Class>;

#define ADD_JSON_PROPERTIES(Class, ...)                                        \
  namespace json {                                                             \
  template <> struct Properties<Class> {                                       \
    constexpr static auto value = std::make_tuple(__VA_ARGS__);                \
  };                                                                           \
  constexpr decltype(Properties<Class>::value) Properties<Class>::value;       \
  }

#define JSON_PROPERTY(Class, Field) property(&Class::Field, #Field)

namespace json {

template <class T> struct JsonParser {
  static T fromJson(const JsonObject &json) {
    T object;
    constexpr auto properties_size =
        std::tuple_size<decltype(Properties<T>::value)>::value;

    PropertyCycle<properties_size, T>::run(Properties<T>::value, object, json);
    /* Using std::move if NRVO don't work */
    return std::move(object);
  }

  static JsonObject toJson(const T &object) {
    JsonObject json;
    constexpr auto properties_size =
        std::tuple_size<decltype(Properties<T>::value)>::value;
    PropertyCycle<properties_size, T>::run(Properties<T>::value, object, json);
    return json;
  }
};

/* Unpack int */
template <> struct JsonValueUnpacker<types::Integer> {
  static constexpr int ERROR = -1;

  static types::Integer unpack(const QJsonValue &val) {
    int result = val.toInt(ERROR);
    if (result == ERROR) {
      throw JsonParseException("Integer unpack error");
    }
    return result;
  }
};

/* Unpack double */
template <> struct JsonValueUnpacker<types::Double> {
  static constexpr double ERROR =
      std::numeric_limits<types::Double>::infinity();

  static types::Double unpack(const QJsonValue &val) {
    double result = val.toDouble(ERROR);
    if (result == ERROR) {
      throw JsonParseException("Double unpack error");
    }
    return result;
  }
};

/* Unpack string */
template <> struct JsonValueUnpacker<types::String> {
  static types::String unpack(const QJsonValue &val) {
    if (!val.isString()) {
      throw JsonParseException("String unpack error");
    }
    return val.toString();
  }
};

/* Unpack pointer */
template <class T> struct JsonValueUnpacker<types::Pointer<T>> {
  static types::Pointer<T> unpack(const QJsonValue &val) {
    auto pointer = types::make_pointer<T>();
    *pointer = JsonValueUnpacker<T>::unpack(val);
    return std::move(pointer);
  }
};

/* Unpack vector */
template <class T> struct JsonValueUnpacker<types::Vector<T>> {
  static types::Vector<T> unpack(const QJsonValue &val) {
    if (!val.isArray()) {
      throw JsonParseException("Vector unpack error");
    }
    auto json_array = val.toArray();

    types::Vector<T> vector{};
    vector.reserve(json_array.size());

    std::transform(json_array.begin(), json_array.end(),
                   nonstd::back_emplacer(vector), JsonValueUnpacker<T>::unpack);
    return std::move(vector);
  }
};

/* Unpack other objects */
template <class T> struct JsonValueUnpacker {
  static T unpack(const QJsonValue &val) {
    if (!val.isObject()) {
      throw JsonParseException("Object unpack error");
    }
    return JsonParser<T>::fromJson(val.toObject());
  }
};

template <class T> struct SimpleJsonPacker {
  static JsonValue pack(const T &value) { return JsonValue(value); }
};

/* Pack int */
template <>
struct JsonValuePacker<types::Integer>
    : public SimpleJsonPacker<types::Integer> {};

/* Pack double */
template <>
struct JsonValuePacker<types::Double> : public SimpleJsonPacker<types::Double> {
};

/* Pack string */
template <>
struct JsonValuePacker<types::String> : public SimpleJsonPacker<types::String> {
};

/* Pack pointer */
template <class T> struct JsonValuePacker<types::Pointer<T>> {
  static JsonValue pack(const types::Pointer<T> &pointer) {
    return JsonValuePacker<T>::pack(*pointer);
  }
};

/* Pack vector */
template <class T> struct JsonValuePacker<types::Vector<T>> {
  static JsonValue pack(const types::Vector<T> &vector) {
    QJsonArray array;
    std::transform(vector.begin(), vector.end(), std::back_inserter(array),
                   JsonValuePacker<T>::pack);
    return array;
  }
};

template <class T> struct JsonValuePacker {
  static JsonValue pack(const T &value) { return JsonParser<T>::toJson(value); }
};
} // namespace json

#endif // !JSON_PARSER_H
