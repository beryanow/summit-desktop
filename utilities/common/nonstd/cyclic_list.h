#ifndef CYCLIC_LIST_H
#define CYCLIC_LIST_H

#include <iterator>
#include <memory>
#include <stdexcept>

namespace nonstd {

template <class T, class Allocator = std::allocator<T>> class CyclicList {
  struct Node {
    Node *next = nullptr;
    Node *prev = nullptr;
    T value;

    explicit Node(const T &value) : value(value) {}
    explicit Node(T &&value) : value(std::move(value)) {}

    template <class... Args>
    Node(Args &&...args) : value(std::forward<Args>(args)...) {}
  };

  class Iterator;

  typename std::allocator_traits<Allocator>::template rebind_alloc<Node>
      allocator{};

  template <class... Args> Node *safe_alloc(Args &&...args) {
    Node *node = allocator.allocate(1);
    try {
      allocator.construct(node, std::forward<Args>(args)...);
    } catch (...) {
      allocator.deallocate(node, 1);
      throw;
    }
    return node;
  }

  template <class... Args> Node *create_node(Node **head, Args &&...args) {
    if (!*head) {
      *head = safe_alloc(std::forward<Args>(args)...);
      (*head)->next = *head;
      (*head)->prev = *head;
      ++_size_;
      return *head;
    }
    Node *node = safe_alloc(std::forward<Args>(args)...);
    node->next = *head;
    node->prev = (*head)->prev;
    (*head)->prev->next = node;
    (*head)->prev = node;
    ++_size_;
    return node;
  }

  void cleanup(Node *head) noexcept {
    Node *current = head;
    if (!current) {
      return;
    }
    do {
      Node *next = current->next;
      allocator.destroy(current);
      allocator.deallocate(current, 1);
      current = next;
    } while (current != head);
    head = nullptr;
    _size_ = 0u;
  }

public:
  using value_type = T;
  using pointer = T *;
  using const_pointer = const T *;
  using reference = T &;
  using const_reference = const T &;
  using size_type = std::size_t;
  using difference_type = std::ptrdiff_t;
  using iterator = Iterator;

  CyclicList() : _head_(nullptr), _size_(0u) {}

  CyclicList(const CyclicList &other) {
    Node *current = other._head_;
    if (!other._head_) {
      return;
    }
    do {
      try {
        create_node(_head_, current->value);
        current = current->next;
      } catch (...) {
        cleanup(_head_);
        throw;
      }
    } while (current != other._head_);
  }

  CyclicList &operator=(const CyclicList &other) {
    if (this == std::addressof(other)) {
      return *this;
    }
    Node *current = other._head_;
    if (current == nullptr) {
      cleanup(_head_);
      return;
    }
    Node *_new_head_ = nullptr;
    do {
      try {
        create_node(_new_head_, current->value);
        current = current->next;
      } catch (...) {
        cleanup(_new_head_);
        throw;
      }
    } while (current != other._head_);
    cleanup(_head_);
    _head_ = _new_head_;
  }

  CyclicList(CyclicList &&other) noexcept : _head_(other._head_) {
    other->_head_ = nullptr;
    other->_size_ = 0u;
  }

  CyclicList &operator=(CyclicList &&other) noexcept {
    if (this == std::addressof(other)) {
      return *this;
    }
    _head_ = other._head_;
    _size_ = other._size_;
    other->_head_ = nullptr;
    other->_size_ = 0u;
    return *this;
  }

  template <class... Args> void emplace_front(Args &&...args) {
    _head_ = create_node(std::addressof(_head_), std::forward<Args>(args)...);
  }

  template <class... Args> void emplace_back(Args &&...args) {
    create_node(std::addressof(_head_), std::forward<Args>(args)...);
  }

  void push_front(const T &value) { emplace_front(value); }
  void push_back(const T &value) { emplace_back(value); }

  void push_front(T &&value) { emplace_front(std::move(value)); }
  void push_back(T &&value) { emplace_back(std::move(value)); }

  reference front() { return _head_->value; }
  const_reference front() const { return _head_->value; }

  reference back() { return _head_->prev->value; }
  const_reference back() const { return _head_->prev->value; }

  void swap(CyclicList &other) noexcept {
    if (this != std::addressof(other)) {
      std::swap(_head_, other._head_);
    }
  }

  void rotate(size_type n) {
    if (n >= size()) {
      throw std::out_of_range("Wrong rotate value");
    }
    for (size_type i = 0; i < n; ++i) {
      _head_ = _head_->next;
    }
  }

  size_type size() const { return _size_; }

  iterator start() noexcept { return iterator(_head_); }

  ~CyclicList() { cleanup(_head_); }

private:
  Node *_head_;
  size_type _size_;

  class Iterator {
    Node *current;

  public:
    using iterator_category = std::bidirectional_iterator_tag;
    using value_type = typename CyclicList::value_type;
    using difference_type = typename CyclicList::difference_type;
    using pointer = typename CyclicList::pointer;
    using reference = value_type &;

    explicit Iterator(Node *head = nullptr) : current(head) {}

    Iterator &operator++() {
      current = current->next;
      return *this;
    }

    Iterator operator++(int) {
      Iterator temp(current);
      ++(*this);
      return temp;
    }

    Iterator &operator--() {
      current = current->prev;
      return *this;
    }

    Iterator operator--(int) {
      Iterator temp(current);
      --(*this);
      return temp;
    }

    friend bool operator==(const Iterator &lhs, const Iterator &rhs) {
      return lhs.current == rhs.current;
    }

    friend bool operator!=(const Iterator &lhs, const Iterator &rhs) {
      return !(lhs.current == rhs.current);
    }

    reference operator*() const { return current->value; }
    pointer operator->() const { return &current->value; }
  };
};

template <class T, class Allocator>
void swap(CyclicList<T, Allocator> &a, CyclicList<T, Allocator> &b) {
  a.swap(b);
}
} // namespace nonstd

#endif // CYCLIC_LIST_H
