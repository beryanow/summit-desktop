#ifndef ERROR_H
#define ERROR_H

#include <cassert>
#include <system_error>
#include <type_traits>
#include <utility>

namespace nonstd {

namespace detail {

template <typename T, typename... Ts> class AlignerImpl {
  T t;
  AlignerImpl<Ts...> rest;
  AlignerImpl() = delete;
};

template <typename T> class AlignerImpl<T> {
  T t;
  AlignerImpl() = delete;
};

template <typename T, typename... Ts> union SizerImpl {
  char arr[sizeof(T)];
  SizerImpl<Ts...> rest;
};

template <typename T> union SizerImpl<T> { char arr[sizeof(T)]; };
} // end namespace detail

/// A suitably aligned and sized character array member which can hold elements
/// of any type.
///
/// These types may be arrays, structs, or any other types. This exposes a
/// `buffer` member which can be used as suitable storage for a placement new of
/// any of these types.
template <typename T, typename... Ts> struct AlignedCharArrayUnion {
  alignas(::nonstd::detail::AlignerImpl<T, Ts...>) char buffer[sizeof(
      nonstd::detail::SizerImpl<T, Ts...>)];
};

template <class ValueType> class error_or {
  template <class OtherT> friend class error_or;

  static constexpr bool isRef = std::is_reference<ValueType>::value;

  using wrap = std::reference_wrapper<std::remove_reference_t<ValueType>>;

public:
  using storage_type = std::conditional_t<isRef, wrap, ValueType>;

private:
  using reference = std::remove_reference_t<ValueType> &;
  using const_reference = const std::remove_reference_t<ValueType> &;
  using pointer = std::remove_reference_t<ValueType> *;
  using const_pointer = const std::remove_reference_t<ValueType> *;

public:
  template <class E>
  error_or(E ErrorCode,
           std::enable_if_t<std::is_error_code_enum<E>::value ||
                                std::is_error_condition_enum<E>::value,
                            void *> = nullptr)
      : HasError(true) {
    new (getErrorStorage()) std::error_code(make_error_code(ErrorCode));
  }

  error_or(std::error_code EC) : HasError(true) {
    new (getErrorStorage()) std::error_code(EC);
  }

  template <class OtherT>
  error_or(OtherT &&Val,
           std::enable_if_t<std::is_convertible<OtherT, ValueType>::value> * =
               nullptr)
      : HasError(false) {
    new (getStorage()) storage_type(std::forward<OtherT>(Val));
  }

  error_or(const error_or &Other) { copyConstruct(Other); }

  template <class OtherT>
  error_or(const error_or<OtherT> &Other,
           std::enable_if_t<std::is_convertible<OtherT, ValueType>::value> * =
               nullptr) {
    copyConstruct(Other);
  }

  template <class OtherT>
  explicit error_or(
      const error_or<OtherT> &Other,
      std::enable_if_t<!std::is_convertible<OtherT, const ValueType &>::value>
          * = nullptr) {
    copyConstruct(Other);
  }

  error_or(error_or &&Other) { moveConstruct(std::move(Other)); }

  template <class OtherT>
  error_or(error_or<OtherT> &&Other,
           std::enable_if_t<std::is_convertible<OtherT, ValueType>::value> * =
               nullptr) {
    moveConstruct(std::move(Other));
  }

  // This might eventually need SFINAE but it's more complex than is_convertible
  // & I'm too lazy to write it right now.
  template <class OtherT>
  explicit error_or(
      error_or<OtherT> &&Other,
      std::enable_if_t<!std::is_convertible<OtherT, ValueType>::value> * =
          nullptr) {
    moveConstruct(std::move(Other));
  }

  error_or &operator=(const error_or &Other) {
    copyAssign(Other);
    return *this;
  }

  error_or &operator=(error_or &&Other) {
    moveAssign(std::move(Other));
    return *this;
  }

  ~error_or() {
    if (!HasError)
      getStorage()->~storage_type();
  }

  /// Return false if there is an error.
  explicit operator bool() const { return !HasError; }

  reference get() { return *getStorage(); }
  const_reference get() const {
    return const_cast<error_or<ValueType> *>(this)->get();
  }

  std::error_code getError() const {
    return HasError ? *getErrorStorage() : std::error_code();
  }

  pointer operator->() { return toPointer(getStorage()); }

  const_pointer operator->() const { return toPointer(getStorage()); }

  reference operator*() { return *getStorage(); }

  const_reference operator*() const { return *getStorage(); }

private:
  template <class OtherT> void copyConstruct(const error_or<OtherT> &Other) {
    if (!Other.HasError) {
      // Get the other value.
      HasError = false;
      new (getStorage()) storage_type(*Other.getStorage());
    } else {
      // Get other's error.
      HasError = true;
      new (getErrorStorage()) std::error_code(Other.getError());
    }
  }

  template <class T1>
  static bool compareThisIfSameType(const T1 &a, const T1 &b) {
    return &a == &b;
  }

  template <class T1, class T2>
  static bool compareThisIfSameType(const T1 &a, const T2 &b) {
    return false;
  }

  template <class OtherT> void copyAssign(const error_or<OtherT> &Other) {
    if (compareThisIfSameType(*this, Other))
      return;

    this->~ErrorOr();
    new (this) error_or(Other);
  }

  template <class OtherT> void moveConstruct(error_or<OtherT> &&Other) {
    if (!Other.HasError) {
      // Get the other value.
      HasError = false;
      new (getStorage()) storage_type(std::move(*Other.getStorage()));
    } else {
      // Get other's error.
      HasError = true;
      new (getErrorStorage()) std::error_code(Other.getError());
    }
  }

  template <class OtherT> void moveAssign(error_or<OtherT> &&Other) {
    if (compareThisIfSameType(*this, Other))
      return;

    this->~ErrorOr();
    new (this) error_or(std::move(Other));
  }

  pointer toPointer(pointer Val) { return Val; }

  const_pointer toPointer(const_pointer Val) const { return Val; }

  pointer toPointer(wrap *Val) { return &Val->get(); }

  const_pointer toPointer(const wrap *Val) const { return &Val->get(); }

  storage_type *getStorage() {
    assert(!HasError && "Cannot get value when an error exists!");
    return reinterpret_cast<storage_type *>(TStorage.buffer);
  }

  const storage_type *getStorage() const {
    assert(!HasError && "Cannot get value when an error exists!");
    return reinterpret_cast<const storage_type *>(TStorage.buffer);
  }

  std::error_code *getErrorStorage() {
    assert(HasError && "Cannot get error when a value exists!");
    return reinterpret_cast<std::error_code *>(ErrorStorage.buffer);
  }

  const std::error_code *getErrorStorage() const {
    return const_cast<error_or<ValueType> *>(this)->getErrorStorage();
  }

  union {
    AlignedCharArrayUnion<storage_type> TStorage;
    AlignedCharArrayUnion<std::error_code> ErrorStorage;
  };
  bool HasError : 1;
};

template <class T, class E>
std::enable_if_t<std::is_error_code_enum<E>::value ||
                     std::is_error_condition_enum<E>::value,
                 bool>
operator==(const error_or<T> &Err, E Code) {
  return Err.getError() == Code;
}
} // namespace nonstd

#endif // ERROR_H
