#include "action_presenter.h"

namespace summit {

ActionPresenter::ActionPresenter(
    ActionView *view, const std::shared_ptr<UseCaseExecutor> &executor)
    : QObject(view), view(view), executor(executor) {}

} // namespace summit
