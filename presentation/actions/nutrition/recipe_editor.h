#ifndef RECIPE_EDIT_H
#define RECIPE_EDIT_H

#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QTextEdit>
#include <QWidget>
#include <model/nutrition/recipe.h>

namespace Ui {
class RecipeEditor;
}

namespace summit {

class IngredientItem : public QWidget {
  Q_OBJECT

  QLabel *name;
  QLabel *amount;

  static constexpr int HEIGHT_SPACING = 10;

public:
  explicit IngredientItem(const QString &name, const QString &amount,
                          QWidget *parent = nullptr);

  QSize sizeHint() const override;
};

class RecipeEditor : public QWidget {
  Q_OBJECT

public:
  explicit RecipeEditor(QWidget *parent = nullptr);
  RecipeEditor(const Recipe &recipe, QWidget *parent = nullptr);

  ~RecipeEditor();

  Recipe &getRecipe();

private slots:
  void on_add_item_clicked();
  void on_remove_item_clicked();

private:
  void addIngredient(const Ingredient &ingredient);

  Recipe recipe;

  QLineEdit *ingredient_name;
  QLineEdit *ingredient_amount;

  QLineEdit *recipe_name;
  QTextEdit *recipe_description;
  QTextEdit *recipe_text;

  QListWidget *recipe_ingredients;
  Ui::RecipeEditor *ui;
};

} // namespace summit

#endif // RECIPE_EDIT_H
