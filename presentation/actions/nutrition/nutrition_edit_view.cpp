#include "nutrition_edit_view.h"

#include "data/nutrition/nutrition_repository.h"
#include "presentation/actions/action_presenter.h"

namespace summit {

NutritionEditView::NutritionEditView(ViewManager *view_manager)
    : ActionView(view_manager, false),
      update_use_case(new UpdateRecipeUseCase(NutritionRepository::create())),
      recipe_editor(new RecipeEditor()), view_manager(view_manager) {
  init();
}

NutritionEditView::NutritionEditView(ViewManager *view_manager,
                                     const Recipe &recipe)
    : ActionView(view_manager, true),
      update_use_case(new UpdateRecipeUseCase(NutritionRepository::create())),
      delete_use_case(new DeleteRecipeUseCase(NutritionRepository::create())),
      recipe_editor(new RecipeEditor(recipe)), view_manager(view_manager) {
  init();

  setSize(recipe.get_size());
  setBackgroundId(recipe.get_backgroundId());
}

void NutritionEditView::onUpdateSuccess() {
  view_manager->prevView();
  ActionView::onUpdateSuccess();
}

void NutritionEditView::init() { getLayout()->addWidget(recipe_editor); }

void NutritionEditView::onAcceptAction() {
  auto &&recipe = recipe_editor->getRecipe();
  recipe.set_size(getSize());
  recipe.set_backgroundId(getBackgroundId());
  getPresenter()->updateData(update_use_case, recipe);
}

void NutritionEditView::onRemoveAction() {
  assert(delete_use_case);

  int id = recipe_editor->getRecipe().get_id();
  getPresenter()->updateData(delete_use_case, id);
}

} // namespace summit
