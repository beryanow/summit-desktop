#include "recipe_editor.h"
#include "ui_recipe_editor.h"

#include "constants/presentation/styles.h"

#include <QCommonStyle>
#include <QLabel>
#include <QScrollBar>

namespace summit {

static const QString RECIPE_NAME_PLACEHOLDER =
    QString::fromUtf8("Введите название...");
static const QString RECIPE_DESC_PLACEHOLDER =
    QString::fromUtf8("Введите описание...");
static const QString RECIPE_TEXT_PLACEHOLDER =
    QString::fromUtf8("Введите рецепт...");
static const QString INGREDIENT_NAME_PLACEHOLDER =
    QString::fromUtf8("Ингредиент...");
static const QString INGREDIENT_AMOUNT_PLACEHOLDER =
    QString::fromUtf8("Количество...");

IngredientItem::IngredientItem(const QString &name, const QString &amount,
                               QWidget *parent)
    : QWidget(parent), name(new QLabel(name)), amount(new QLabel(amount)) {
  QHBoxLayout *layout = new QHBoxLayout;

  layout->addWidget(this->name);
  layout->addWidget(this->amount);

  setLayout(layout);
}

QSize IngredientItem::sizeHint() const {
  QSize size = QWidget::sizeHint();
  size.setHeight(size.height() + HEIGHT_SPACING);
  return size;
}

RecipeEditor::RecipeEditor(QWidget *parent)
    : QWidget(parent), ui(new Ui::RecipeEditor) {
  ui->setupUi(this);

  recipe.set_ingredients(std::vector<Ingredient>{});

  recipe_name = ui->recipe_name;
  recipe_name->setPlaceholderText(RECIPE_NAME_PLACEHOLDER);

  recipe_description = ui->recipe_description;
  recipe_description->setPlaceholderText(RECIPE_DESC_PLACEHOLDER);
  recipe_description->verticalScrollBar()->setStyle(new QCommonStyle);
  recipe_description->verticalScrollBar()->setStyleSheet(
      VERTICAL_SCROLLBAR_STYLE);

  recipe_text = ui->recipe_text;
  recipe_text->setPlaceholderText(RECIPE_TEXT_PLACEHOLDER);
  recipe_text->verticalScrollBar()->setStyle(new QCommonStyle);
  recipe_text->verticalScrollBar()->setStyleSheet(VERTICAL_SCROLLBAR_STYLE);

  recipe_ingredients = ui->recipe_edit;
  recipe_ingredients->setWordWrap(true);
  recipe_ingredients->verticalScrollBar()->setStyle(new QCommonStyle);
  recipe_ingredients->verticalScrollBar()->setStyleSheet(
      VERTICAL_SCROLLBAR_STYLE);

  ingredient_name = ui->ingredient_name;
  ingredient_name->setPlaceholderText(INGREDIENT_NAME_PLACEHOLDER);

  ingredient_amount = ui->ingredient_amount;
  ingredient_amount->setPlaceholderText(INGREDIENT_AMOUNT_PLACEHOLDER);
}

RecipeEditor::RecipeEditor(const Recipe &recipe, QWidget *parent)
    : RecipeEditor(parent) {
  this->recipe = recipe;

  const auto &ingredients = recipe.get_ingredients();
  std::for_each(
      ingredients.begin(), ingredients.end(),
      [this](const Ingredient &ingredient) { addIngredient(ingredient); });

  recipe_name->setText(recipe.get_name());
  recipe_description->setText(recipe.get_description());
  recipe_text->setText(recipe.get_recipe());
}

RecipeEditor::~RecipeEditor() { delete ui; }

Recipe &RecipeEditor::getRecipe() {
  recipe.set_name(recipe_name->text());
  recipe.set_description(recipe_description->toPlainText());
  recipe.set_recipe(recipe_text->toPlainText());
  return recipe;
}

} // namespace summit

void summit::RecipeEditor::on_add_item_clicked() {
  QString name = ingredient_name->text();
  QString amount = ingredient_amount->text();
  if (name.isEmpty() || amount.isEmpty()) {
    return;
  }
  Ingredient ingredient;
  ingredient.set_name(name);
  ingredient.set_amount(amount);

  recipe.get_ingredients().push_back(ingredient);
  addIngredient(ingredient);

  ingredient_name->clear();
  ingredient_amount->clear();
}

void summit::RecipeEditor::on_remove_item_clicked() {
  int index = recipe_ingredients->currentRow();
  if (index < 0) {
    return;
  }

  QListWidgetItem *item = recipe_ingredients->takeItem(index);
  delete item;

  auto &ingredients = recipe.get_ingredients();
  ingredients.erase(ingredients.begin() + index);
}

void summit::RecipeEditor::addIngredient(const summit::Ingredient &ingredient) {
  QListWidgetItem *item = new QListWidgetItem(recipe_ingredients);
  auto ingredient_widget =
      new IngredientItem(ingredient.get_name(), ingredient.get_amount());
  ingredient_widget->setSizePolicy(QSizePolicy::Expanding,
                                   QSizePolicy::Preferred);
  item->setSizeHint(ingredient_widget->sizeHint());

  recipe_ingredients->setItemWidget(item, ingredient_widget);
}
