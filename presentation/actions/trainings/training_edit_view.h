#ifndef TRAINING_EDIT_VIEW_H
#define TRAINING_EDIT_VIEW_H

#include "domain/trainings/delete_training_usecase.h"
#include "domain/trainings/update_training_usecase.h"
#include "model/trainings/training.h"
#include "presentation/actions/action_view.h"
#include "training_editor.h"

#include <QWidget>

namespace summit {

class TrainingEditView : public ActionView {
  Q_OBJECT

public:
  explicit TrainingEditView(ViewManager *view_manager);
  TrainingEditView(ViewManager *view_manager, const Training &trainig,
                   bool is_editable = true);

  void onAcceptAction() override;
  void onRemoveAction() override;

  ~TrainingEditView();

private:
  void onUpdateSuccess() override;

  bool is_editable;

  void init();

  TrainingEditor *editor;
  std::shared_ptr<UpdateTrainingUseCase> update_use_case;
  std::shared_ptr<DeleteTrainingUseCase> delete_use_case;

  ViewManager *view_manager;
};

} // namespace summit

#endif // TRAINING_EDIT_VIEW_H
