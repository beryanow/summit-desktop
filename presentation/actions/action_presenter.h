#ifndef ACTIONPRESENTER_H
#define ACTIONPRESENTER_H

#include <QObject>

#include "action_view.h"
#include "domain/common/usecase_executor.h"

namespace summit {

class ActionPresenter : public QObject {
  Q_OBJECT
public:
  ActionPresenter(ActionView *view,
                  const std::shared_ptr<UseCaseExecutor> &executor);

  template <class UseCaseType, class... Args>
  inline void updateData(std::shared_ptr<UseCaseType> &use_case,
                         Args &&...args) {
    disconnect(use_case.get(), nullptr, this, nullptr);

    connect(use_case.get(), &UseCaseType::finished, this,
            [this]() { view->onUpdateSuccess(); });

    connect(use_case.get(), &UseCaseType::failed, this,
            [this](const DomainError &error) {
              view->onUpdateFailure(PresentationError{error.getMessage()});
            });

    executor->execute(use_case, std::forward<Args>(args)...);
  }

private:
  ActionView *view;
  std::shared_ptr<UseCaseExecutor> executor;
};

} // namespace summit

#endif // ACTIONPRESENTER_H
