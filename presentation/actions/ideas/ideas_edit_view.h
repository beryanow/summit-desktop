#ifndef IDEAS_EDIT_VIEW_H
#define IDEAS_EDIT_VIEW_H

#include <QLineEdit>
#include <QTextEdit>

#include "domain/ideas/delete_idea_usecase.h"
#include "domain/ideas/update_idea_usecase.h"
#include "model/ideas/idea.h"
#include "presentation/actions/action_view.h"
#include "presentation/common/view_manager.h"
#include "utilities/common/nonstd/optional.h"

namespace summit {

class IdeasEditView : public ActionView {
  Q_OBJECT

public:
  /* Calls to create new idea */
  explicit IdeasEditView(ViewManager *view_manager);

  /* Calls to edit existing idea */
  explicit IdeasEditView(ViewManager *view_manager, const Idea &idea);

  void onUpdateSuccess() override;

private:
  void init();

  void onAcceptAction() override;
  void onRemoveAction() override;

  nonstd::optional<Idea> idea;

  std::shared_ptr<UpdateIdeaUseCase> update_use_case;
  std::shared_ptr<DeleteIdeaUseCase> delete_use_case;

  QLineEdit *idea_name;
  QTextEdit *idea_content;

  ViewManager *view_manager;
};

} // namespace summit

#endif // IDEAS_EDIT_VIEW_H
