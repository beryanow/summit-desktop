#ifndef TASKACTIONS_H
#define TASKACTIONS_H

#include "task_action_factory.h"

namespace summit {

enum TASK_STAGE : std::uint8_t { New = 0, InProgress = 1, Resolved = 2 };

static const std::array<QString, 3> TASK_STAGES = {"New", "In progress",
                                                   "Done"};

class NewTask : public TaskStateHandler {
  static const QString description;

public:
  NewTask();

  std::unique_ptr<TaskStateHandler> nextAction() const override;
  void handle(Task &task) override;
};

class InProgressTask : public TaskStateHandler {
  static const QString description;

public:
  InProgressTask();

  std::unique_ptr<TaskStateHandler> nextAction() const override;
  void handle(Task &task) override;
};

class ResolvedTask : public TaskStateHandler {
  static const QString description;

public:
  ResolvedTask();

  std::unique_ptr<TaskStateHandler> nextAction() const override;
  void handle(Task &task) override;
};

} // namespace summit
#endif // TASKACTIONS_H
