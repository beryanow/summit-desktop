#ifndef TASKACTIONFACTORY_H
#define TASKACTIONFACTORY_H

#include "model/tasks/task.h"

#include <memory>
#include <unordered_map>

namespace summit {

class TaskStateHandler {

  QString description;

public:
  void setDescription(const QString &description) {
    this->description = description;
  }

  const QString &getDescription() const { return description; }

  virtual void handle(Task &task) = 0;
  virtual std::unique_ptr<TaskStateHandler> nextAction() const = 0;

  virtual ~TaskStateHandler() = default;
};

class AbstractTaskCreator {
public:
  virtual std::unique_ptr<TaskStateHandler> create() = 0;
  virtual ~AbstractTaskCreator() = default;
};

template <class TaskType> class TaskCreator : public AbstractTaskCreator {
public:
  std::unique_ptr<TaskStateHandler> create() override {
    return std::make_unique<TaskType>();
  }
};

class TaskActionFactory {

  using CreatorPointer = std::unique_ptr<AbstractTaskCreator>;
  std::unordered_map<QString, CreatorPointer> creators;

public:
  template <class TaskType> void add(const QString &key) {
    creators[key] = std::make_unique<TaskCreator<TaskType>>();
  }

  std::unique_ptr<TaskStateHandler> create(const QString &key) const {
    auto it = creators.find(key);
    if (it == creators.end()) {
      return nullptr;
    }
    return it->second->create();
  }
};

} // namespace summit

#endif // TASKACTIONFACTORY_H
