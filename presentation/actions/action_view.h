#ifndef ACTIONVIEW_H
#define ACTIONVIEW_H

#include <QDialog>
#include <QVBoxLayout>

#include "model/images/image.h"
#include "presentation/common/base_view.h"
#include "presentation/common/presentation_error.h"
#include "presentation/common/view_manager.h"
#include "presentation/images/images_viewer.h"

namespace Ui {
class ActionView;
}

namespace summit {

class ActionPresenter;

class ActionView : public QDialog,
                   protected ImageSubscriber,
                   private ImagesView {

  constexpr static int DEFAULT_BACKGROUND_ID = 1;

public:
  ActionView(ViewManager *view_manager, bool has_data = false);

  virtual ~ActionView();

  virtual void onUpdateSuccess();
  virtual void onUpdateFailure(const PresentationError &error);

  void setSizeSelectorVisible(bool value);
  void setImageSelectorVisible(bool value);

  void setAcceptButtonVisible(bool value);
  void setRemoveButtonVisible(bool value);

  void setCustomBackgroundPlaceholder(QPushButton *placeholder);

protected:
  void createPresenter() override;

  QVBoxLayout *getLayout();
  ActionPresenter *getPresenter();

  virtual void onAcceptAction() = 0;
  virtual void onRemoveAction() = 0;

  void setSize(const QString &size);
  QString getSize() const;

  void setImage(const ImagePointer &image) override;
  void setBackgroundId(int id);
  int getBackgroundId() const;

private:
  void blockingAction(bool block);
  void prepareOnAcceptAction();
  void prepareOnRemoveAction();

  void onImageSendSuccess(const ImagePointer &image) override;
  void onImageActionFailure(const PresentationError &error) override;

  void selectBackgroundImage();

  std::atomic<int> background_id;
  QPushButton *background_placeholder;
  ImagePointer background_image = nullptr;

  ActionPresenter *presenter;

  ViewManager *view_manager;
  Ui::ActionView *ui;
};

} // namespace summit

#endif // ACTIONVIEW_H
