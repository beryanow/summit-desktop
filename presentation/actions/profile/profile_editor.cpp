#include "profile_editor.h"
#include "ui_profile_editor.h"

#include <QIntValidator>

namespace summit {

static const QString NAME_EDIT_PLACEHOLDER = QString::fromUtf8("Имя");
static const QString SURNAME_EDIT_PLACEHOLDER = QString::fromUtf8("Фамилия");
static const QString AGE_EDIT_PLACEHOLDER = QString::fromUtf8("Возраст");

static const QString USERNAME_EDIT_PLACEHOLDER =
    QString::fromUtf8("Имя пользователя");
static const QString EMAIL_EDIT_PLACEHOLDER = QString::fromUtf8("Почта");

ProfileEditor::ProfileEditor(const UserProfilePointer &profile, QWidget *parent)
    : QWidget(parent), profile(profile), ui(new Ui::ProfileEditor) {

  ui->setupUi(this);

  ui->name_line_edit->setText(profile->get_name());
  ui->name_line_edit->setPlaceholderText(NAME_EDIT_PLACEHOLDER);

  ui->surname_line_edit->setText(profile->get_surname());
  ui->surname_line_edit->setPlaceholderText(SURNAME_EDIT_PLACEHOLDER);

  ui->age_line_edit->setText(QString::number(profile->get_age()));
  ui->age_line_edit->setPlaceholderText(AGE_EDIT_PLACEHOLDER);
  ui->age_line_edit->setValidator(new QIntValidator(0, 100, this));
}

ProfileEditor::~ProfileEditor() { delete ui; }

QPushButton *ProfileEditor::getAvatarPlaceholder() { return ui->avatar_button; }

UserProfilePointer ProfileEditor::getProfile() {
  profile->set_name(ui->name_line_edit->text());
  profile->set_surname(ui->surname_line_edit->text());
  profile->set_age(ui->age_line_edit->text().toInt());
  return profile;
}

} // namespace summit
