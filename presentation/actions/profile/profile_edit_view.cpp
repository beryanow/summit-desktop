#include "profile_edit_view.h"
#include "data/profile/profile_repository.h"
#include "presentation/actions/action_presenter.h"

namespace summit {

ProfileEditView::ProfileEditView(ViewManager *view_manager,
                                 const UserProfilePointer &profile)
    : ActionView(view_manager, true), editor(new ProfileEditor(profile)),
      use_case(new UpdateProfileUseCase(ProfileRepository::create())),
      view_manager(view_manager) {

  setSizeSelectorVisible(false);
  setImageSelectorVisible(false);
  setRemoveButtonVisible(false);

  getLayout()->addWidget(editor);

  setCustomBackgroundPlaceholder(editor->getAvatarPlaceholder());
  setBackgroundId(profile->get_imageId());
}

void ProfileEditView::onUpdateSuccess() {
  view_manager->prevView();
  ActionView::onUpdateSuccess();
}

void ProfileEditView::onAcceptAction() {
  auto &&profile = editor->getProfile();
  profile->set_imageId(getBackgroundId());

  getPresenter()->updateData(use_case, profile);
}

void ProfileEditView::onRemoveAction() {
  qWarning() << "Unsupported operation";
}

} // namespace summit
