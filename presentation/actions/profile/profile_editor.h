#ifndef PROFILE_EDITOR_H
#define PROFILE_EDITOR_H

#include <QPushButton>
#include <QWidget>

#include "model/profile/user_profile.h"

namespace Ui {
class ProfileEditor;
}

namespace summit {

class ProfileEditor : public QWidget {
  Q_OBJECT

public:
  explicit ProfileEditor(const UserProfilePointer &profile,
                         QWidget *parent = nullptr);
  ~ProfileEditor();

  QPushButton *getAvatarPlaceholder();

  UserProfilePointer getProfile();

private:
  UserProfilePointer profile;

  Ui::ProfileEditor *ui;
};

} // namespace summit

#endif // PROFILE_EDITOR_H
