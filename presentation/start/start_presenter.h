#ifndef QUOTESTORAGE_H
#define QUOTESTORAGE_H

#include "presentation/common/label/transparent_label.h"
#include "presentation/start/start_contract.h"
#include "utilities/common/nonstd/cyclic_list.h"

#include "domain/common/usecase_executor.h"
#include "domain/start/quote_transition_usecase.h"

#include <QDebug>
#include <QMutex>
#include <QObject>
#include <QSharedPointer>

namespace summit {

class StartPresenter : public QObject, public StartContract::Presenter {
  Q_OBJECT

  using QuotePointer = QSharedPointer<const Quote>;
  QMutex mutex;

  nonstd::CyclicList<QuotePointer> quotes;
  nonstd::CyclicList<QuotePointer>::iterator current_quote;

  StartContract::View *view;

  std::shared_ptr<UseCaseExecutor> executor;
  std::shared_ptr<QuoteTransitionUseCase> use_case;

public:
  StartPresenter(StartContract::View *view,
                 const std::shared_ptr<UseCaseExecutor> &executor);

  void ready();

  void addQuote(const Quote &quote);

  void transitionLeft() override;
  void transitionRight() override;

  void resize(const QSize &size);

  ~StartPresenter();

private:
  void runTransition(const QuotePointer &from, const QuotePointer &to);
  bool isBusy() const;
};

} // namespace summit

#endif // QUOTESTORAGE_H
