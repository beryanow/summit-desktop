#ifndef START_CONTRACT_H
#define START_CONTRACT_H

#include "presentation/common/base_presenter.h"
#include "presentation/common/base_view.h"

#include "model/start/quote.h"

namespace summit {
class StartContract {
public:
  class Presenter : public IBasePresenter {
  public:
    virtual void transitionLeft() = 0;
    virtual void transitionRight() = 0;
  };

  class View : public IBaseView {
  public:
    virtual void display(const QSharedPointer<const Quote> &from,
                         const QSharedPointer<const Quote> &to,
                         double opacity) = 0;
    virtual void display(const QSharedPointer<const Quote> &quote) = 0;
  };
};
} // namespace summit

#endif // START_CONTRACT_H
