#include "start_view.h"
#include "ui_start_view.h"

#include "domain/common/single_usecase_executor.h"
#include "presentation/common/font/font.h"
#include "presentation/sign_in/sign_in_view.h"
#include "presentation/sign_up/sign_up_view.h"
#include "presentation/start/start_presenter.h"
#include "utilities/common/nonstd/cyclic_list.h"

#include <QPainter>
#include <QResizeEvent>
#include <QThread>
#include <mutex>

using namespace summit;

namespace {
QString QUOTE_LABEL_STYLE =
    "BlindingLabel {color:white; background-color:transparent;"
    "text-align:center;}";
}

namespace summit {

StartView::StartView(ViewManager *view_manager)
    : QDialog(view_manager), view_manager(view_manager), ui(new Ui::StartView) {
  ui->setupUi(this);

  quote_label = new TransparentLabel;
  quote_label->setStyleSheet(QUOTE_LABEL_STYLE);
  quote_label->setFont(QFont(DEFAULT_HEADER_FONT_NAME, 16));

  ui->QuotePlaceholder->addWidget(quote_label);

  createPresenter();
}

void StartView::resizeEvent(QResizeEvent *) {
  std::lock_guard<QMutex> lock(mutex);
  if (quote_data.from) {
    image_buffer[0] = quote_data.from->getImage()->getScaled(size());
  }
  if (quote_data.to) {
    image_buffer[1] = quote_data.to->getImage()->getScaled(size());
  }
}

void StartView::paintEvent(QPaintEvent *) {
  QPainter painter(this);
  painter.setFont(QFont(DEFAULT_HEADER_FONT_NAME, 24));

  {
    std::lock_guard<QMutex> lock(mutex);

    if (nullptr == quote_data.to) {
      if (quote_data.from) {
        displayCentered(painter, image_buffer[0]);
        quote_label->setText(quote_data.from->getText());
      }
    } else {
      qreal A = quote_data.opacity;
      qreal B = 1.0 - A;

      /* Draw quote text */
      qreal text_opacity = (A > B) ? A : B;
      text_opacity = 2.0 * text_opacity - 1.0;

      quote_label->setText((A > B) ? quote_data.from->getText()
                                   : quote_data.to->getText());
      quote_label->setOpacity(text_opacity);
      quote_label->setWordWrap(true);

      /* Draw background */
      painter.setOpacity(A);
      displayCentered(painter, image_buffer[0]);

      painter.setOpacity(B);
      displayCentered(painter, image_buffer[1]);
    }
  }
}

void StartView::display(const QuotePointer &from, const QuotePointer &to,
                        double opacity) {
  {
    std::lock_guard<QMutex> lock(mutex);

    quote_data.from = from;
    quote_data.to = to;
    quote_data.opacity = opacity;

    /* Save scaled images */
    image_buffer[0] = from->getImage()->getScaled(size());
    image_buffer[1] = to->getImage()->getScaled(size());
  }
  repaint();
}

void StartView::display(const QuotePointer &quote) {
  {
    std::lock_guard<QMutex> lock(mutex);

    quote_data.from = quote;
    quote_data.to = nullptr;
    quote_data.opacity = 1.0;

    /* Save scaled image */
    image_buffer[0] = quote->getImage()->getScaled(size());
  }
  repaint();
}

void StartView::createPresenter() {
  presenter = new StartPresenter(this, SingleUseCaseExecutor::getInstance());
}

void StartView::on_rotate_left_clicked() { presenter->transitionLeft(); }

void StartView::on_rotate_right_clicked() { presenter->transitionRight(); }

StartView::~StartView() { delete ui; }

void StartView::on_sign_up_button_clicked() {
  SignUpView *sign_up = new SignUpView(view_manager);
  {
    std::lock_guard<QMutex> lock(mutex);
    if (quote_data.to) {
      sign_up->setBackgroundImage(quote_data.to->getImage());
    } else if (quote_data.from) {
      sign_up->setBackgroundImage(quote_data.from->getImage());
    }
  }
  view_manager->nextView(sign_up);
}

void StartView::on_sign_in_button_clicked() {
  SignInView *sign_in = new SignInView(view_manager);
  {
    std::lock_guard<QMutex> lock(mutex);
    if (quote_data.to) {
      sign_in->setBackgroundImage(quote_data.to->getImage());
    } else if (quote_data.from) {
      sign_in->setBackgroundImage(quote_data.from->getImage());
    }
  }
  view_manager->nextView(sign_in);
}

} // namespace summit
