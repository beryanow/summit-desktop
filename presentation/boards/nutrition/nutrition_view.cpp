#include "nutrition_view.h"
#include "data/nutrition/nutrition_repository.h"
#include "domain/common/single_usecase_executor.h"
#include "presentation/boards/board_presenter.h"

#include "presentation/actions/nutrition/nutrition_edit_view.h"
#include "presentation/common/widget/scrollable_widgets_viewer.h"
#include "recipe_widget_view_adapter.h"

namespace summit {

NutritionView::NutritionView(ViewManager *view_manager)
    : BoardView(view_manager),
      use_case(new LoadRecipesUseCase(NutritionRepository::create())),
      view_manager(view_manager) {}

NutritionView::~NutritionView() {}

void NutritionView::onShowAction() {
  getPresenter()->loadAllData<Recipe>(
      use_case, [this](const Recipe &recipe, Widget *widget) {
        addImageSubscriber(recipe.get_backgroundId(), widget);
        connect(widget, &Widget::clicked, this, [this, recipe]() {
          this->view_manager->nextView(
              new NutritionEditView(this->view_manager, recipe));
        });
      });
}

void NutritionView::onAddAction() {
  view_manager->nextView(new NutritionEditView(view_manager));
}

} // namespace summit
