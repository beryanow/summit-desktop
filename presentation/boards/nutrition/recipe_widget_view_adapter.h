#ifndef RECIPEWIDGETVIEWADAPTER_H
#define RECIPEWIDGETVIEWADAPTER_H

#include "model/nutrition/recipe.h"
#include "presentation/common/widget/widgets_viewer_adapter.h"

namespace summit {

class RecipeWidgetViewAdapter : public WidgetsViewerAdapter<Recipe> {
public:
  nonstd::optional<WidgetData>
  createWidget(const Recipe &object) const override;
};

} // namespace summit

#endif // RECIPEWIDGETVIEWADAPTER_H
