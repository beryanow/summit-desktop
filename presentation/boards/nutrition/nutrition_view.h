#ifndef NUTRITIONVIEW_H
#define NUTRITIONVIEW_H

#include "domain/nutrition/load_recipes_usecase.h"
#include "presentation/boards/board_view.h"
#include "presentation/common/view_manager.h"
#include "recipe_widget_view_adapter.h"

#include <QDialog>

namespace summit {

template <> struct WidgetAdapterTraits<Recipe> {
  using type = RecipeWidgetViewAdapter;
};

class NutritionView : public BoardView {
  Q_OBJECT

public:
  static constexpr const char *TITLE = "#Рецепты";

  explicit NutritionView(ViewManager *view_manager);
  ~NutritionView();

private:
  void onShowAction() override;
  void onAddAction() override;

  std::shared_ptr<LoadRecipesUseCase> use_case;

  ViewManager *view_manager;
};

} // namespace summit

#endif // NUTRITIONVIEW_H
