#include "recipe_widget_view_adapter.h"

#include "constants/presentation/widget_constants.h"
#include "presentation/common/font/font.h"
#include "presentation/common/widget/widget.h"
#include "presentation/common/widget/widget_utilities.h"

namespace summit {
nonstd::optional<RecipeWidgetViewAdapter::WidgetData>
RecipeWidgetViewAdapter::createWidget(const Recipe &recipe) const {

  QSize widget_size;
  if (auto size_optional = widgetSizeFromString(recipe.get_size())) {
    widget_size = size_optional.value();
  } else {
    qWarning() << "Wrong widget size";
    return nonstd::nullopt;
  }
  auto widget_type = computeWidgetType(widget_size);

  Widget *widget =
      new Widget(widget_type, nullptr, WIDGET_BORDER_RADIUS, recipe.get_name());
  widget->setBaseSize(WIDGET_BASE_SIZE[widget_type]);
  widget->setHeaderFont(WIDGET_HEADER_FONT);

  int item_count = 0;
  for (auto &task : recipe.get_ingredients()) {
    if (++item_count > WIDGET_ITEM_MAX_COUNT[widget_type]) {
      break;
    }
    TitledWidgetItem *item =
        new TitledWidgetItem(task.get_name(), task.get_amount());
    item->setTitleDefaultFont(ITEM_TITLE_FONT);
    item->setBodyDefaultFont(ITEM_BODY_FONT);
    item->setTitleTextColor(Qt::white);
    item->setBodyTextColor(Qt::white);
    item->setStripeColor(Qt::red);
    item->isDisplayBody(widget_type != SMALL);

    widget->addItem(item);
  }

  return WidgetData{widget, widget_size};
}

} // namespace summit
