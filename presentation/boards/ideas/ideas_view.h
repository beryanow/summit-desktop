#ifndef IDEAS_VIEW_H
#define IDEAS_VIEW_H

#include "domain/ideas/load_ideas_usecase.h"
#include "idea_widget_view_adapter.h"
#include "presentation/boards/board_view.h"
#include "presentation/common/view_manager.h"

#include <QDialog>

namespace Ui {
class IdeasView;
}

namespace summit {

template <> struct WidgetAdapterTraits<Idea> {
  using type = IdeaWidgetViewerAdapter;
};

class IdeasView : public BoardView {
  Q_OBJECT

public:
  static constexpr const char *TITLE = "#Мысли";

  explicit IdeasView(ViewManager *view_manager);
  ~IdeasView();

private:
  void onShowAction() override;
  void onAddAction() override;

  std::shared_ptr<LoadIdeasUseCase> use_case;

  ViewManager *view_manager;
  Ui::IdeasView *ui;
};

} // namespace summit

#endif // IDEAS_VIEW_H
