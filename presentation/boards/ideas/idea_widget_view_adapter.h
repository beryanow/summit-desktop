#ifndef IGEA_WIDGET_VIEW_ADAPTER_H
#define IGEA_WIDGET_VIEW_ADAPTER_H

#include "model/ideas/idea.h"
#include "presentation/common/widget/widgets_viewer_adapter.h"

namespace summit {

class IdeaWidgetViewerAdapter : public WidgetsViewerAdapter<Idea> {
public:
  nonstd::optional<WidgetData> createWidget(const Idea &idea) const override;
};
} // namespace summit

#endif // IGEA_WIDGET_VIEW_ADAPTER_H
