#include "idea_widget_view_adapter.h"

#include "constants/presentation/widget_constants.h"
#include "presentation/common/font/font.h"
#include "presentation/common/widget/widget.h"
#include "presentation/common/widget/widget_utilities.h"

namespace summit {

nonstd::optional<IdeaWidgetViewerAdapter::WidgetData>
IdeaWidgetViewerAdapter::createWidget(const Idea &idea) const {

  QSize widget_size;
  if (auto size_optional = widgetSizeFromString(idea.get_size())) {
    widget_size = size_optional.value();
  } else {
    qWarning() << "Wrong widget size";
    return nonstd::nullopt;
  }
  auto widget_type = computeWidgetType(widget_size);

  Widget *widget =
      new Widget(widget_type, nullptr, WIDGET_BORDER_RADIUS, idea.get_name());
  widget->setBaseSize(WIDGET_BASE_SIZE[widget_type]);
  widget->setHeaderFont(WIDGET_HEADER_FONT);

  WidgetItem *item = new WidgetItem(idea.get_content());
  item->setBodyDefaultFont(ITEM_BODY_FONT);
  item->setBodyTextColor(Qt::white);
  item->setStripeColor(Qt::white);
  widget->addItem(item);
  return WidgetData{widget, widget_size};
}

} // namespace summit
