#include "training_list_widget_view_adapter.h"

#include <algorithm>
#include <map>

namespace summit {

nonstd::optional<TrainingListWidgetViewAdapter::WidgetData>
TrainingListWidgetViewAdapter::createWidget(
    const TrainingList &training_list) const {

  constexpr QSize widget_size{2, 2};
  constexpr auto widget_type = WidgetType::LARGE;
  constexpr auto MAX_TRAINIGS = 2;

  Widget *widget = new Widget(widget_type, nullptr, WIDGET_BORDER_RADIUS,
                              QString::fromUtf8("Ближайшие тренировки"));
  widget->setBaseSize(WIDGET_BASE_SIZE[widget_type]);
  widget->setHeaderFont(WIDGET_HEADER_FONT);

  std::multimap<QDate, std::reference_wrapper<const Training>> timetable;

  for (auto &training : training_list) {
    for (auto &schedule : training.get_trainingSchedules()) {
      timetable.insert(
          std::make_pair(QDate::fromString(schedule.get_date(), Qt::ISODate),
                         std::cref(training)));
    }
  }
  QDate current = QDate::currentDate();
  auto iter = timetable.lower_bound(current);
  if (iter == timetable.end()) {
    /* No nearest trainings */
    auto item = std::make_unique<WidgetItem>("Нет ближайших тренировок");
    item->setBodyDefaultFont(ITEM_BODY_FONT);
    item->setBodyTextColor(Qt::white);
    item->setStripeColor(Qt::transparent);
    widget->addItem(item.release());
  } else {
    /* Add two nearest trainings */
    for (int i = 0; i < MAX_TRAINIGS && iter != timetable.end(); ++i) {
      QString trainig_desc;
      auto &training = iter->second.get();
      for (auto &exercise : training.get_exercises()) {
        trainig_desc.append(exercise.get_description()).append('\n');
      }
      auto item = std::make_unique<TitledWidgetItem>(iter->first.toString(),
                                                     trainig_desc.trimmed());
      item->setTitleDefaultFont(ITEM_TITLE_FONT);
      item->setBodyDefaultFont(ITEM_BODY_FONT);
      item->setTitleTextColor(Qt::white);
      item->setBodyTextColor(Qt::white);
      item->setStripeColor(Qt::white);
      widget->addItem(item.release());

      ++iter;
    }
  }
  return WidgetData{widget, widget_size};
}

} // namespace summit
