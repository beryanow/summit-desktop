#include "trainings_view.h"

#include "data/trainings/trainings_repository.h"
#include "presentation/actions/trainings/training_edit_view.h"
#include "presentation/boards/board_presenter.h"
#include "training_list_view.h"

namespace summit {

TrainingsView::TrainingsView(ViewManager *view_manager)
    : BoardView(view_manager),
      /* Init background images */
      TRAINING_TIMETABLE_BACKGROUND(
          fromFile(":/binary/resource/training_window/timetable.png")),
      GYM_DEAFULT_TRAINIG_BACKGROUND(
          fromFile(":/binary/resource/training_window/gym.png")),
      STREET_DEAFULT_TRAINIG_BACKGROUND(
          fromFile(":/binary/resource/training_window/street.png")),

      /* Init stuff */
      load_all_trainings_use_case(
          new LoadTrainingListUseCase(TrainingsRepository::create())),
      view_manager(view_manager) {

  load_default_training_use_case[TrainingType::GYM].reset(
      new LoadDefaultTrainingUseCase(TrainingsRepository::create()));
  load_default_training_use_case[TrainingType::STREET].reset(
      new LoadDefaultTrainingUseCase(TrainingsRepository::create()));
}

void TrainingsView::onShowAction() {
  getPresenter()->loadSingleData<TrainingList>(
      load_all_trainings_use_case,
      [this](const TrainingList &trainigs, Widget *widget) {
        widget->setImage(TRAINING_TIMETABLE_BACKGROUND);
        connect(widget, &Widget::clicked, this, [this, trainigs]() {
          view_manager->nextView(new TrainingListView(view_manager));
        });
      },
      TRAINING_TIMETABLE_WIDGET_POS);

  getPresenter()->loadSingleData<Training>(
      load_default_training_use_case[TrainingType::GYM],
      [this](const Training &training, Widget *widget) {
        /* Set default background */
        widget->setImage(GYM_DEAFULT_TRAINIG_BACKGROUND);
        connect(widget, &Widget::clicked, this, [this, training]() {
          view_manager->nextView(
              new TrainingEditView(view_manager, training, false));
        });
      },
      DEFAILT_GYM_TRAINING_WIDGET_POS, TrainingType::GYM);

  getPresenter()->loadSingleData<Training>(
      load_default_training_use_case[TrainingType::STREET],
      [this](const Training &training, Widget *widget) {
        /* Set default background */
        widget->setImage(STREET_DEAFULT_TRAINIG_BACKGROUND);
        connect(widget, &Widget::clicked, this, [this, training]() {
          view_manager->nextView(
              new TrainingEditView(view_manager, training, false));
        });
      },
      DEFAILT_STREET_TRAINING_WIDGET_POS, TrainingType::STREET);
}

void TrainingsView::onAddAction() {
  view_manager->nextView(new TrainingEditView(view_manager));
}

} // namespace summit
