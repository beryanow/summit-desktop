#include "training_widget_view_adapter.h"

#include "constants/presentation/widget_constants.h"
#include "presentation/common/font/font.h"
#include "presentation/common/widget/widget.h"
#include "presentation/common/widget/widget_utilities.h"

namespace summit {

nonstd::optional<TrainingWidgetViewAdapter::WidgetData>
TrainingWidgetViewAdapter::createWidget(const Training &training) const {

  constexpr QSize widget_size{2, 2};
  constexpr auto widget_type = WidgetType::LARGE;

  Widget *widget = new Widget(widget_type, nullptr, WIDGET_BORDER_RADIUS,
                              training.get_name());
  widget->setBaseSize(WIDGET_BASE_SIZE[widget_type]);
  widget->setHeaderFont(WIDGET_HEADER_FONT);

  int item_count = 0;
  for (auto &exercise : training.get_exercises()) {
    if (++item_count > WIDGET_ITEM_MAX_COUNT[widget_type]) {
      break;
    }
    TitledWidgetItem *item =
        new TitledWidgetItem(exercise.get_name(), exercise.get_description());
    item->setTitleDefaultFont(ITEM_TITLE_FONT);
    item->setBodyDefaultFont(ITEM_BODY_FONT);
    item->setTitleTextColor(Qt::white);
    item->setBodyTextColor(Qt::white);
    item->setStripeColor(Qt::red);

    widget->addItem(item);
  }
  return WidgetData{widget, widget_size};
}

} // namespace summit
