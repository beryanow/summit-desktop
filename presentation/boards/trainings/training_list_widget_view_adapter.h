#ifndef TRAINIG_LIST_WIDGET_VIEW_ADAPTER_H
#define TRAINIG_LIST_WIDGET_VIEW_ADAPTER_H

#include "model/trainings/training.h"
#include "presentation/common/widget/widgets_viewer_adapter.h"

namespace summit {

class TrainingListWidgetViewAdapter
    : public WidgetsViewerAdapter<TrainingList> {
public:
  nonstd::optional<WidgetData>
  createWidget(const TrainingList &training_list) const override;
};

} // namespace summit

#endif // TRAINIG_LIST_WIDGET_VIEW_ADAPTER_H
