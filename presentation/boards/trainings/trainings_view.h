#ifndef TRAINIGSVIEW_H
#define TRAINIGSVIEW_H

#include "domain/trainings/load_default_training_usecase.h"
#include "domain/trainings/load_training_list_usecase.h"
#include "presentation/boards/board_view.h"
#include "presentation/common/view_manager.h"
#include "training_list_widget_view_adapter.h"
#include "training_widget_view_adapter.h"

namespace summit {

class TrainingsView : public BoardView {
  Q_OBJECT

  static constexpr QPoint TRAINING_TIMETABLE_WIDGET_POS{0, 1};
  static constexpr QPoint DEFAILT_GYM_TRAINING_WIDGET_POS{0, 3};
  static constexpr QPoint DEFAILT_STREET_TRAINING_WIDGET_POS{0, 5};

  ImagePointer TRAINING_TIMETABLE_BACKGROUND;
  ImagePointer GYM_DEAFULT_TRAINIG_BACKGROUND;
  ImagePointer STREET_DEAFULT_TRAINIG_BACKGROUND;

public:
  static constexpr const char *TITLE = "#Тренировки";

  explicit TrainingsView(ViewManager *view_manager);
  ~TrainingsView() = default;

private:
  void onShowAction() override;
  void onAddAction() override;

  std::shared_ptr<LoadTrainingListUseCase> load_all_trainings_use_case;
  std::shared_ptr<LoadDefaultTrainingUseCase> load_default_training_use_case[2];

  ViewManager *view_manager;
};

} // namespace summit

#endif // TRAINIGSVIEW_H
