#ifndef PROFILE_WIDGET_VIEW_ADAPTER_H
#define PROFILE_WIDGET_VIEW_ADAPTER_H

#include "model/profile/user_profile.h"
#include "presentation/common/widget/widgets_viewer_adapter.h"

namespace summit {

class ProfileWidgetViewAdapter
    : public WidgetsViewerAdapter<UserProfilePointer> {

public:
  nonstd::optional<WidgetData>
  createWidget(const UserProfilePointer &profile) const override;
};

} // namespace summit

#endif // PROFILE_WIDGET_VIEW_ADAPTER_H
