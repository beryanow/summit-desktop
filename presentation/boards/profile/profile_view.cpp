#include "profile_view.h"
#include "presentation/boards/board_presenter.h"

#include "data/profile/profile_repository.h"
#include "data/profile/progress_repository.h"
#include "presentation/actions/profile/profile_edit_view.h"
#include "presentation/settings/presentation_settings.h"

#include "presentation/boards/goals/goals_view.h"
#include "presentation/boards/ideas/ideas_view.h"
#include "presentation/boards/nutrition/nutrition_view.h"

namespace summit {

ProfileView::ProfileView(ViewManager *view_manager)
    : BoardView(view_manager),
      /* UseCase */
      load_profile_usecase(new LoadProfileUseCase(ProfileRepository::create())),
      load_progress_usecase(
          new LoadProgressUseCase(ProgressRepository::create())),
      default_progress_image(
          fromFile(":/binary/resource/profile_window/progress.png")),
      view_manager(view_manager) {}

ProfileView::~ProfileView() {}

void ProfileView::onShowAction() {
  getPresenter()->loadSingleData<UserProfilePointer>(
      load_profile_usecase,
      [this](const UserProfilePointer &profile, Widget *widget) {
        addImageSubscriber(profile->get_imageId(), widget);

        auto &settings = PresentationSettings::getInstance();
        auto &settingsDto = profile->get_settingsDto();

        settings.setImageId<ProfileView>(
            settingsDto.get_profileScreenBackgroundId());
        settings.setImageId<GoalsView>(
            settingsDto.get_goalsScreenBackgroundId());
        settings.setImageId<IdeasView>(
            settingsDto.get_ideasScreenBackgroundId());
        settings.setImageId<NutritionView>(
            settingsDto.get_nutritionScreenBackgroundId());

        connect(widget, &Widget::clicked, this, [this, profile]() {
          view_manager->nextView(new ProfileEditView(view_manager, profile));
        });
      },
      PROFILE_WIDGET_POS);

  /*getPresenter()->loadSingleData<UserProgressList>(
      load_progress_usecase,
      [this](const UserProgressList &progress_list, Widget *widget) {
        if (progress_list.empty()) {
          widget->setImage(default_progress_image);
          return;
        }
        const UserProgress &progress = progress_list.back();
        addImageSubscriber(progress.get_imageId(), widget);
      });*/
}

} // namespace summit
