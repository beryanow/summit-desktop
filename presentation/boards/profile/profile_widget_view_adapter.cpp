#include "profile_widget_view_adapter.h"

#include "constants/presentation/widget_constants.h"
#include "presentation/common/font/font.h"
#include "presentation/common/widget/widget.h"
#include "presentation/common/widget/widget_utilities.h"

namespace summit {

static TitledWidgetItem *constructItem(const QString &title,
                                       const QString &text) {
  TitledWidgetItem *item = new TitledWidgetItem(title, text);
  item->setTitleDefaultFont(ITEM_TITLE_FONT);
  item->setBodyDefaultFont(ITEM_BODY_FONT);
  item->setTitleTextColor(Qt::white);
  item->setBodyTextColor(Qt::white);
  item->setStripeColor(Qt::red);
  return item;
}

nonstd::optional<ProfileWidgetViewAdapter::WidgetData>
ProfileWidgetViewAdapter::createWidget(
    const UserProfilePointer &profile) const {

  constexpr QSize widget_size{2, 2};
  constexpr auto widget_type = WidgetType::LARGE;

  Widget *widget =
      new Widget(widget_type, nullptr, WIDGET_BORDER_RADIUS, "Профиль");
  widget->setBaseSize(WIDGET_BASE_SIZE[WidgetType::LARGE]);
  widget->setHeaderFont(WIDGET_HEADER_FONT);

  widget->addItem(constructItem("Имя", profile->get_name()));
  widget->addItem(constructItem("Фамилия", profile->get_surname()));
  widget->addItem(
      constructItem("Возраст", QString::number(profile->get_age())));

  return WidgetData{widget, widget_size};
}

} // namespace summit
