#ifndef DEFAULTPRESENTER_H
#define DEFAULTPRESENTER_H

#include <QObject>

#include "board_view.h"
#include "domain/common/usecase_executor.h"

namespace summit {

class BoardPresenter : public QObject {
  Q_OBJECT

public:
  explicit BoardPresenter(BoardView *view,
                          const std::shared_ptr<UseCaseExecutor> &executor);

  /* Load with callback */
  template <class DataType, class UseCaseType, class CallbackType,
            class... Args>
  void loadSingleData(
      std::shared_ptr<UseCaseType> &use_case, CallbackType callback,
      const nonstd::optional<QPoint> &widget_position = nonstd::nullopt,
      Args &&...args);

  template <class DataType, class UseCaseType, class CallbackType,
            class... Args>
  void loadAllData(std::shared_ptr<UseCaseType> &use_case,
                   CallbackType callback, Args &&...args);

private:
  BoardView *view;
  std::shared_ptr<UseCaseExecutor> executor;
};

template <class DataType, class UseCaseType, class CallbackType, class... Args>
void BoardPresenter::loadSingleData(
    std::shared_ptr<UseCaseType> &use_case, CallbackType callback,
    const nonstd::optional<QPoint> &widget_position, Args &&...args) {
  disconnect(use_case.get(), nullptr, this, nullptr);

  connect(use_case.get(),
          /* Finished signal */
          &UseCaseType::finished,
          /* On success */
          this, [this, callback, widget_position](const DataType &data) {
            view->showData<DataType>({data}, callback, widget_position);
          });

  connect(use_case.get(),
          /* Failed signal */
          &UseCaseType::failed,
          /* On failure */
          this, [this](const DomainError &error) {
            view->showError(PresentationError{error.getMessage()});
          });

  executor->execute(use_case, std::forward<Args>(args)...);
}

template <class DataType, class UseCaseType, class CallbackType, class... Args>
void BoardPresenter::loadAllData(std::shared_ptr<UseCaseType> &use_case,
                                 CallbackType callback, Args &&...args) {
  disconnect(use_case.get(), nullptr, this, nullptr);

  connect(use_case.get(),
          /* Finished signal */
          &UseCaseType::finished,
          /* On success */
          this, [this, callback](const QList<DataType> &data) {
            view->showData<DataType>(data, callback);
          });

  connect(use_case.get(),
          /* Failed signal */
          &UseCaseType::failed,
          /* On failure */
          this, [this](const DomainError &error) {
            view->showError(PresentationError{error.getMessage()});
          });

  executor->execute(use_case, std::forward<Args>(args)...);
}

} // namespace summit

#endif // DEFAULTPRESENTER_H
