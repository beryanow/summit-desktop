#ifndef WINDOWMANAGER_H
#define WINDOWMANAGER_H

#include <QStackedWidget>
#include <stack>

class ViewManager : public QWidget {
  Q_OBJECT

  struct WidgetData {
    QWidget *widget;
    bool do_deallocation;
  };

  QLayout *layout;

  QStackedWidget *manager;
  std::stack<WidgetData> widgets_data;

public:
  ViewManager(QWidget *parent);
  void nextView(QWidget *scene, bool do_deallocation = true);
  void setIndex(int idx);

  void update();
  void prevView();

  void removeWidgets();

  QLayout *getLayout();
};

#endif // WINDOWMANAGER_H
