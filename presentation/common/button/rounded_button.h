#ifndef ASPECTRATIOBUTTON_H
#define ASPECTRATIOBUTTON_H

#include <QPushButton>

namespace summit {

class RoundedButton : public QPushButton {
  Q_OBJECT

public:
  RoundedButton(QWidget *parent = nullptr);

private:
  QFont getScaledFont() const;

protected:
  void resizeEvent(QResizeEvent *) override;
  void paintEvent(QPaintEvent *) override;
};

} // namespace summit

#endif // ASPECTRATIOBUTTON_H
