#ifndef IMAGE_SUBSCRIBER_H
#define IMAGE_SUBSCRIBER_H

#include "model/images/image.h"

namespace summit {

class ImageSubscriber {
public:
  virtual void setImage(const ImagePointer &image) = 0;
  virtual ~ImageSubscriber() = default;
};

} // namespace summit

#endif // IMAGE_SUBSCRIBER_H
