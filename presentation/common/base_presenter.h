#ifndef BASE_PRESENTER_H
#define BASE_PRESENTER_H

class IBasePresenter {
public:
  virtual ~IBasePresenter() = default;
};

#endif // BASE_PRESENTER_H
