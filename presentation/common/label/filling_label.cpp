#include "filling_label.h"
#include "presentation/common/font/font.h"

#include <QPainter>
#include <QResizeEvent>
#include <QStyle>
#include <QTimer>

#include <QDebug>

namespace summit {

static qreal getExpansionRatio(const QSize &new_size, const QSize &old_size) {
  if (!old_size.isValid()) {
    return 1.0;
  }
  double old_rect = std::min(old_size.width(), old_size.height());
  double new_rect = std::min(new_size.width(), new_size.height());
  return new_rect / old_rect;
}

int TextFillingLabel::counter = 0;

TextFillingLabel::TextFillingLabel(QWidget *parent) : QLabel(parent) {}

TextFillingLabel::TextFillingLabel(const QString &text, QWidget *parent)
    : QLabel(parent) {
  setText(text);
  id = counter++;
}

/*QSize TextFillingLabel::sizeHint() const {
  QFontMetrics const fontMetrics(font());
  QRect const r = fontMetrics.boundingRect(
      QRect(QPoint(0, 0), size()), Qt::TextWordWrap | Qt::ElideRight, content);
  return QSize(width(), r.height());
}*/

void TextFillingLabel::resizeEvent(QResizeEvent *event) {
  QLabel::resizeEvent(event); // Process the event. The label is now resized
}

void TextFillingLabel::paintEvent(QPaintEvent *event) {
  // QPainter painter(this);

  // QString text = fontMetrics().elidedText(text, Qt::ElideRight, width());

  // if (!text.isEmpty()) {
  //  painter.drawText(rect(), Qt::AlignLeft | Qt::TextWordWrap, text);
  //}
}

int ScaleTextlabel::counter = 0;

ScaleTextlabel::ScaleTextlabel(QWidget *parent, Qt::WindowFlags f)
    : QLabel(parent, f) {
  id = counter++;
  initTimer();
}
ScaleTextlabel::ScaleTextlabel(const QString &text, QWidget *parent,
                               Qt::WindowFlags f)
    : QLabel(text, parent, f) {
  id = counter++;
  initTimer();
}

void ScaleTextlabel::resizeEvent(QResizeEvent *event) {
  QLabel::resizeEvent(event);
  return;

  if (!DEFAULT_SIZE) {
    DEFAULT_SIZE = event->size();
    fontSize = font().pointSizeF();
  }
  QLabel::resizeEvent(event);

  qDebug() << "Label" << id
           << getExpansionRatio(event->size(), event->oldSize());

  setStyleSheet("color:transparent");
  m_resizeTimer->start();
}

void ScaleTextlabel::resizeFont() {
  QFont font = this->font();
  qreal ratio = getExpansionRatio(this->size(), DEFAULT_SIZE.value());

  int new_size = static_cast<int>(fontSize.value() * ratio);
  if (new_size < MIN_FONT_SIZE) {
    new_size = MIN_FONT_SIZE;
  }

  font.setPointSizeF(new_size);
  setFont(font);

  setStyleSheet("color:white");
  return;
}

void ScaleTextlabel::initTimer() {
  m_resizeTimer = new QTimer(this);
  m_resizeTimer->setInterval(100);
  m_resizeTimer->setSingleShot(true);
  connect(m_resizeTimer, &QTimer::timeout, this, &ScaleTextlabel::resizeFont);
}

ScaleTextlabel *create(QString text) { return new ScaleTextlabel(text); }

} // namespace summit
