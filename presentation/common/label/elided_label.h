#ifndef ELIDEDLABEL_H
#define ELIDEDLABEL_H

#include <QLabel>
#include <QObject>

namespace summit {

class ElidedLabel : public QLabel {
  Q_OBJECT
  Q_PROPERTY(QString text READ text WRITE setText)
  Q_PROPERTY(bool isElided READ isElided)

public:
  explicit ElidedLabel(const QString &text, QWidget *parent = nullptr);

  void setText(const QString &text);
  const QString &text() const { return content; }
  bool isElided() const { return elided; }

  void setColor(const QColor &color);
  const QColor &getColor() const;

  QSize sizeHint() const override;

protected:
  void paintEvent(QPaintEvent *event) override;

private:
  bool init = false;

  bool elided;
  QString content;

  QColor text_color = Qt::black;
};

} // namespace summit

#endif // ELIDEDLABEL_H
