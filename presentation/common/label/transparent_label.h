#ifndef BLINDINGLABEL_H
#define BLINDINGLABEL_H

#include <QLabel>
#include <QWidget>

#define OPACITY_MAX 1.0
#define OPACITY_MIN 0.0

namespace summit {

class TransparentLabel : public QLabel {
  Q_OBJECT

  qreal opacity = OPACITY_MAX;

public:
  explicit TransparentLabel(QWidget *parent = nullptr);
  void setOpacity(qreal opacity);
  qreal getOpacity() const;

protected:
  void paintEvent(QPaintEvent *) override;
};

} // namespace summit

#endif // BLINDINGLABEL_H
