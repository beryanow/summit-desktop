#include "elided_label.h"

#include <QDebug>
#include <QPainter>
#include <QStyle>
#include <QTextLayout>

namespace summit {

ElidedLabel::ElidedLabel(const QString &text, QWidget *parent)
    : QLabel(text, parent), elided(false), content(text) {
  setWordWrap(true);
  setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
  setStyleSheet("background:transparent; color:transparent");
}

void ElidedLabel::setText(const QString &newText) {
  content = newText;
  update();
}

void ElidedLabel::setColor(const QColor &color) { text_color = color; }

const QColor &ElidedLabel::getColor() const { return text_color; }

QSize ElidedLabel::sizeHint() const { return QLabel::sizeHint(); }

void ElidedLabel::paintEvent(QPaintEvent *) {
  QPainter painter(this);
  QFontMetrics fontMetrics = painter.fontMetrics();

  bool didElide = false;
  int lineSpacing = fontMetrics.lineSpacing();
  int y = 0;

  painter.setFont(font());
  painter.setPen(text_color);

  bool running = true;

  auto strings = content.split(QRegExp("[\\n\\t\\r]"));
  for (const auto &string : strings) {
    if (!running) {
      break;
    }
    QTextLayout textLayout(string, painter.font());
    textLayout.beginLayout();
    forever {
      QTextLine line = textLayout.createLine();

      if (!line.isValid())
        break;

      line.setLineWidth(width());
      int nextLineY = y + lineSpacing;

      if (height() >= nextLineY + lineSpacing) {
        line.draw(&painter, QPoint(0, y));
        y = nextLineY;
      } else {
        QString lastLine = string.mid(line.textStart());
        QString elidedLastLine =
            fontMetrics.elidedText(lastLine, Qt::ElideRight, width());
        painter.drawText(QPoint(0, y + fontMetrics.ascent()), elidedLastLine);
        line = textLayout.createLine();
        didElide = line.isValid();
        running = false;
        break;
      }
    }
    textLayout.endLayout();
  }
  if (y + lineSpacing < height()) {
    resize(width(), y + lineSpacing);
  }

  if (didElide != elided) {
    elided = didElide;
  }
}

} // namespace summit
