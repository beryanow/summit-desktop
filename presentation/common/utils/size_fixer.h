#ifndef SIZEFIXER_H
#define SIZEFIXER_H

#include <QWidget>

namespace summit {

class SizeFixer : public QWidget {

  Q_OBJECT

  QWidget *wrapper;
  double aspect_ratio;

public:
  explicit SizeFixer(QWidget *parent = nullptr);
  virtual ~SizeFixer();

  void setWidget(QWidget *widget);
  QWidget *getWidget();

  void setAspectRatio(double aspect_ratio);
  double getAspectRatio() const;

protected:
  virtual QSize sizeHint() const override;
  virtual void resizeEvent(QResizeEvent *event) override;

private:
  virtual void fixSize() = 0;
};

class WidthForHeightFixer : public SizeFixer {
public:
  explicit WidthForHeightFixer(QWidget *parent = nullptr);

private:
  void fixSize() override;
};

class HeightForWidthFixer : public SizeFixer {
public:
  explicit HeightForWidthFixer(QWidget *parent = nullptr);

private:
  void fixSize() override;
};

QWidget *heigthForWidth(QWidget *widget, double ratio);
QWidget *widthForHeight(QWidget *widget, double ratio);

} // namespace summit

#endif // SIZEFIXER_H
