#ifndef WIDGET_UTILITIES_H
#define WIDGET_UTILITIES_H

#include "constants/presentation/widget_constants.h"
#include "presentation/common/widget/widget.h"
#include "utilities/common/nonstd/optional.h"

#include <regex>

namespace summit {

nonstd::optional<QSize> widgetSizeFromString(const QString &string);

WidgetType computeWidgetType(const QSize &size);

QWidget *wrapWidget(WidgetType type, Widget *widget);

} // namespace summit

#endif // WIDGET_UTILITIES_H
