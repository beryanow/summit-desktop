#include "widget_item.h"

#include "presentation/common/font/font.h"
#include "presentation/common/label/elided_label.h"
#include "presentation/common/label/filling_label.h"

#include <QDebug>
#include <QPainter>
#include <QResizeEvent>
#include <QTextLayout>
#include <QTimer>

namespace summit {

const QString WidgetItem::STRIPE_STYLE =
    "QLabel{border-radius:2px; background-color:rgba(%1,%2,%3,%4);}";

WidgetItem::WidgetItem(const QString &text, QWidget *parent)
    : QFrame(parent), stripe(new QLabel), body(new ElidedLabel(text)),
      body_default_font(DEFAULT_TEXT_FONT_NAME, MIN_FONT_SIZE),
      content_frame(new QFrame), content_layout(new QVBoxLayout) {
  /* Content */
  content_frame->setLayout(content_layout);
  content_frame->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);

  body->setWordWrap(true);
  body->setAlignment(Qt::AlignTop);

  content_layout->addWidget(body);
  content_layout->setSpacing(CONTENT_SPACING);
  content_layout->setMargin(0);

  /* Stripe parameters */
  stripe->setMaximumWidth(STRIPE_WIDTH);

  /* Widget layout */
  QHBoxLayout *widget_layout = new QHBoxLayout;
  widget_layout->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
  widget_layout->setMargin(STRIPE_WIDTH);
  widget_layout->addWidget(stripe);
  widget_layout->addWidget(content_frame);

  setLayout(widget_layout);
}

WidgetItem::~WidgetItem() = default;

void WidgetItem::setBodyDefaultFont(const QFont &font) {
  body_default_font = font;
  body->setFont(body_default_font);
}

const QFont &WidgetItem::getBodyFont() const { return body->font(); }

void WidgetItem::setBodyText(const QString &text) { body->setText(text); }

QString WidgetItem::getBodyText() const { return body->text(); }

void WidgetItem::setBodyTextColor(const QColor &color) {
  body->setColor(color);
}

const QColor &WidgetItem::getBodyTextColor() const { return body->getColor(); }

void WidgetItem::setStripeColor(const QColor &color) {
  stripe_color = color;

  QString style = STRIPE_STYLE.arg(
      QString::number(color.red()), QString::number(color.green()),
      QString::number(color.blue()), QString::number(color.alpha()));
  stripe->setStyleSheet(style);
}

const QColor &WidgetItem::getStripeColor() { return stripe_color; }

QBoxLayout *WidgetItem::getLayout() const { return content_layout; }

QLabel *WidgetItem::getBody() const { return body; }

void WidgetItem::paintEvent(QPaintEvent *event) { QFrame::paintEvent(event); }

void WidgetItem::showEvent(QShowEvent *event) {
  QFrame::showEvent(event);
  layout()->invalidate();
}

void WidgetItem::onExpansion(qreal ratio) {
  body->setFont(expandFont(body_default_font, ratio));
}

TitledWidgetItem::TitledWidgetItem(const QString &title_text,
                                   const QString &body_text, QWidget *parent)
    : WidgetItem(body_text, parent), title(new ElidedLabel(title_text)),
      title_default_font(DEFAULT_TEXT_FONT_NAME, MIN_FONT_SIZE) {

  title->setWordWrap(true);
  title->setAlignment(Qt::AlignTop);

  getLayout()->insertWidget(0, title);
}

void TitledWidgetItem::isDisplayBody(bool value) {
  getBody()->setVisible(value);
}

bool TitledWidgetItem::isDisplayBody() const { return getBody()->isVisible(); }

void TitledWidgetItem::setTitleDefaultFont(const QFont &font) {
  title_default_font = font;
  title->setFont(title_default_font);
}

const QFont &TitledWidgetItem::getTitleFont() const { return title->font(); }

void TitledWidgetItem::setTitleText(const QString &text) {
  title->setText(text);
}

QString TitledWidgetItem::getTitleText() const { return title->text(); }

void TitledWidgetItem::setTitleTextColor(const QColor &color) {
  title->setColor(color);
}

void TitledWidgetItem::onExpansion(qreal ratio) {
  WidgetItem::onExpansion(ratio);
  title->setFont(expandFont(title_default_font, ratio));
}

} // namespace summit
