#ifndef WIDGETSVIEWER_H
#define WIDGETSVIEWER_H

#include <QDebug>
#include <QGridLayout>
#include <QScrollArea>
#include <QTableWidget>

#include "widgets_viewer.h"

namespace summit {

class ScrollableWidgetsViewer : public QScrollArea, public WidgetsViewer {
  static constexpr int HEIGHT_MARGIN = 20;
  static constexpr qreal WIDGET_MARGIN_RATIO = 50.0;

  QFrame *frame;
  QGridLayout *layout;

public:
  explicit ScrollableWidgetsViewer(int row_count, QWidget *parent = nullptr);

  void addWidgetAuto(QWidget *widget,
                     Qt::Alignment alignment = Qt::Alignment()) override;
  void addWidgetAuto(QWidget *widget, int rowSpan, int columnSpan,
                     Qt::Alignment alignment = Qt::Alignment()) override;

  void addWidget(QWidget *widget, int row, int column,
                 Qt::Alignment alignment = Qt::Alignment()) override;
  void addWidget(QWidget *widget, int row, int column, int rowSpan,
                 int columnSpan,
                 Qt::Alignment alignment = Qt::Alignment()) override;

  void setPosition(int position) override;
  int getPosition() const override;

  void clear() override;

protected:
  void resizeEvent(QResizeEvent *event) override;
  void wheelEvent(QWheelEvent *event) override;

private:
  void updateStretch(int row_begin, int row_end, int column_begin,
                     int column_end);
  void updateSize();
};

} // namespace summit

#endif // WIDGETSVIEWER_H
