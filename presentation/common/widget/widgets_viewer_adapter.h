#ifndef WIDGET_CREATOR_H
#define WIDGET_CREATOR_H

#include "widget.h"
#include "widget_utilities.h"
#include "widgets_viewer.h"

#include "utilities/common/nonstd/optional.h"

namespace summit {

template <class UserType> class WidgetsViewerAdapter {
public:
  using WidgetData = std::pair<Widget *, QSize>;

  /* Create widget from user type and add them to viewer */
  virtual nonstd::optional<WidgetData>
  createWidget(const UserType &object) const = 0;

  template <class CallbackType>
  void addWidget(WidgetsViewer *viewer, const UserType &object,
                 CallbackType &&callback) {
    nonstd::optional<WidgetData> widget_data_optional = createWidget(object);
    if (!widget_data_optional) {
      qWarning() << "Couldn't add widget";
      return;
    }

    Widget *widget = widget_data_optional->first;
    QSize widget_size = widget_data_optional->second;
    std::forward<CallbackType>(callback)(object, widget);

    viewer->addWidgetAuto(wrapWidget(widget->getType(), widget),
                          widget_size.height(), widget_size.width());
  }

  template <class CallbackType>
  void addWidget(WidgetsViewer *viewer, const UserType &object,
                 CallbackType &&callback, int row, int column) {
    nonstd::optional<WidgetData> widget_data_optional = createWidget(object);
    if (!widget_data_optional) {
      qWarning() << "Couldn't add widget";
      return;
    }

    Widget *widget = widget_data_optional->first;
    QSize widget_size = widget_data_optional->second;
    std::forward<CallbackType>(callback)(object, widget);

    viewer->addWidget(wrapWidget(widget->getType(), widget), row, column,
                      widget_size.height(), widget_size.width());
  }

  virtual ~WidgetsViewerAdapter() = default;
};

} // namespace summit

#endif // WIDGET_CREATOR_H
