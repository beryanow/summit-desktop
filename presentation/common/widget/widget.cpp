#include "widget.h"

#include "presentation/common/font/font.h"
#include "presentation/common/utils/size_fixer.h"

#include <QBitmap>
#include <QDebug>
#include <QLabel>
#include <QPainter>
#include <QPainterPath>
#include <QResizeEvent>
#include <QTimer>

namespace summit {

#define STRETCH 1

static qreal getExpansionRatio(const QSize &new_size, const QSize &old_size) {
  if (!old_size.isValid()) {
    return 1.0;
  }
  double old_rect = std::min(old_size.width(), old_size.height());
  double new_rect = std::min(new_size.width(), new_size.height());
  return new_rect / old_rect;
}

const QString Widget::WIDGET_DEFAULT_STYLE =
    "QWidget{ background-color:transparent; }";

const QColor Widget::DIMMING_COLOR = qRgb(48, 48, 48);

Widget::Widget(WidgetType type, const ImagePointer &background_wrapper,
               qreal border_radius, const QString &header_text, QWidget *parent)
    : QFrame(parent), type(type), widget_layout(new QVBoxLayout),
      items_layout(new QVBoxLayout()), default_border_radius(border_radius),
      current_border_radius(border_radius),
      header(new ElidedLabel(header_text)),
      header_font(QFont(DEFAULT_TEXT_FONT_NAME, MIN_FONT_SIZE)),
      background_wrapper(background_wrapper) {
  /* Init styles */
  setStyleSheet(WIDGET_DEFAULT_STYLE);

  /* Make header */
  QFont font(DEFAULT_TEXT_FONT_NAME);
  font.setWeight(QFont::Bold);

  header->setFont(font);
  header->setColor(Qt::white);
  header->setWordWrap(true);

  /* Items frame */
  QFrame *frame = new QFrame();
  frame->setLayout(items_layout);
  items_layout->setMargin(0);
  items_layout->setSpacing(0);
  items_layout->setAlignment(Qt::AlignTop);

  /* Setup Widget layout */
  widget_layout->addWidget(header);
  widget_layout->addWidget(frame);
  widget_layout->setSpacing(0);
  widget_layout->setMargin(0);
  widget_layout->setAlignment(Qt::AlignTop);

  setLayout(widget_layout);
}

void Widget::addItem(WidgetItem *item) {
  connect(this, &Widget::expandChildren, item, &WidgetItem::onExpansion);
  items_layout->addWidget(item);
}

void Widget::setHeaderFont(const QFont &font) {
  header_font = font;
  header->setFont(header_font);
}

void Widget::setImage(const ImagePointer &image) {
  background_wrapper = image;
  if (image) {
    background = updateBackgroundPixmap();
  }
  repaint();

  emit imageChanged();
}

const ImagePointer &Widget::getImage() const { return background_wrapper; }

void Widget::showEvent(QShowEvent *event) { QFrame::showEvent(event); }

void Widget::resizeEvent(QResizeEvent *event) {
  QFrame::resizeEvent(event);

  /*Background */
  if (background_wrapper) {
    background = updateBackgroundPixmap();
  }

  /* Compute new Widget bounds */
  qreal expansion_ratio = getExpansionRatio(size(), baseSize());
  const qreal shift = BORDER_WIDTH * 2.0;
  widget_bounds =
      QRectF(BORDER_WIDTH, BORDER_WIDTH, width() - shift, height() - shift);

  current_border_radius = default_border_radius * expansion_ratio;

  int margin = static_cast<int>(current_border_radius / MARGIN_RATIO);
  widget_layout->setMargin(margin);

  /* Fonts */
  resizeHeader(expansion_ratio);
  emit expandChildren(expansion_ratio);
}

void Widget::paintEvent(QPaintEvent *) {
  QPainter painter(this);

  painter.setPen(Qt::NoPen);
  painter.setRenderHint(QPainter::Antialiasing);

  /* Draw border */
  QPainterPath path;

  QRectF border_bounds = widget_bounds;
  border_bounds.setHeight(border_bounds.height() + BORDER_WIDTH);
  border_bounds.setWidth(border_bounds.width() - BORDER_SHIFT * 2);
  border_bounds.setX(border_bounds.x() + BORDER_SHIFT);

  path.addRoundedRect(border_bounds, current_border_radius,
                      current_border_radius);

  painter.setBrush(Qt::white);
  painter.drawPath(path);

  /* Draw image */
  if (background.isNull()) {
    painter.setBrush(Qt::gray);
  } else {
    painter.setBrush(background);
  }
  painter.drawRoundedRect(widget_bounds, current_border_radius,
                          current_border_radius);

  /* Dimming */
  painter.setOpacity(BACKGROUND_ALPHA);
  painter.setBrush(DIMMING_COLOR);
  painter.drawRoundedRect(widget_bounds, current_border_radius,
                          current_border_radius);
}

void Widget::mousePressEvent(QMouseEvent *) {
  expandBounds(-EXPANSION_DELTA);
  repaint();
}

void Widget::mouseReleaseEvent(QMouseEvent *) {
  expandBounds(EXPANSION_DELTA);
  repaint();

  /* Clicked signal */
  emit clicked();
}

void Widget::expandBounds(qreal delta) {
  widget_bounds.setX(widget_bounds.x() - delta);
  widget_bounds.setY(widget_bounds.y() - delta);

  widget_bounds.setWidth(widget_bounds.width() + 2.0 * delta);
  widget_bounds.setHeight(widget_bounds.height() + 2.0 * delta);
}

void Widget::resizeHeader(qreal ratio) {
  header->setFont(expandFont(header_font, ratio));
}

QPixmap Widget::updateBackgroundPixmap() {
  auto widget_size = size();
  return background_wrapper->getScaled(widget_size)
      .fit(widget_size)
      .getAsPixmap();
}

#define SQUARE_RATIO 1.0
#define HORIZONTAL_RATIO 2.0

QWidget *squareWidget(QWidget *widget) {
  return widthForHeight(widget, SQUARE_RATIO);
}

QWidget *horizontalWidget(QWidget *widget) {
  return widthForHeight(widget, HORIZONTAL_RATIO);
}

} // namespace summit
