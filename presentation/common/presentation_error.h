#ifndef PRESENTATIONERROR_H
#define PRESENTATIONERROR_H

#include <QString>

class PresentationError {
  QString message;

public:
  PresentationError();

  explicit PresentationError(const QString &message);

  const QString &what() const;
};

#endif // PRESENTATIONERROR_H
