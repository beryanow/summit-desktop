#ifndef FONT_H
#define FONT_H

#include <QFont>
#include <QString>

namespace summit {
constexpr static int MIN_FONT_SIZE = 4;

static const char *DEFAULT_HEADER_FONT_NAME = "mr_SilverAgeQueensG";
static const char *DEFAULT_HEADER_FONT_PATH = ":/fonts/resource/title_font.ttf";

static const char *DEFAULT_TEXT_FONT_NAME = "Verdana";

void loadFonts();
} // namespace summit

#endif // FONT_H
