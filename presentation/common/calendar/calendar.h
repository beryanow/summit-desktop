#ifndef CALENDAR_H
#define CALENDAR_H

#include <QCalendarWidget>
#include <QDialog>

namespace Ui {
class Calendar;
}

namespace summit {

class CalendarWidget : public QCalendarWidget {
  Q_OBJECT

  QList<QDate> selected_dates;

public:
  explicit CalendarWidget(QWidget *parent = 0);
  ~CalendarWidget();

  void setDates(const QList<QDate> &dates);

  QList<QDate> getDates() const;

protected:
  virtual void paintCell(QPainter *painter, const QRect &rect,
                         const QDate &date) const;
};

class Calendar : public QDialog {
  Q_OBJECT

public:
  explicit Calendar(QWidget *parent = nullptr);
  explicit Calendar(const QList<QDate> &dates, QWidget *parent = 0);
  ~Calendar();

  QList<QDate> getDates() const;

private slots:
  void on_exit_button_clicked();

private:
  Ui::Calendar *ui;
};

} // namespace summit

#endif // CALENDAR_H
