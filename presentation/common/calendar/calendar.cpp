#include "calendar.h"
#include "ui_calendar.h"

#include <QDebug>
#include <QPainter>

namespace summit {

CalendarWidget::CalendarWidget(QWidget *parent) : QCalendarWidget(parent) {

  setGridVisible(false);
  connect(this, &QCalendarWidget::clicked, this, [this](const QDate &date) {
    if (selected_dates.contains(date)) {
      selected_dates.removeAll(date);
    } else {
      selected_dates.push_back(date);
    }
  });
}

CalendarWidget::~CalendarWidget() {}

void CalendarWidget::setDates(const QList<QDate> &dates) {
  selected_dates = dates;
}

QList<QDate> CalendarWidget::getDates() const {
  auto sorted_dates = selected_dates;
  std::sort(sorted_dates.begin(), sorted_dates.end());
  return sorted_dates;
}

void CalendarWidget::paintCell(QPainter *painter, const QRect &rect,
                               const QDate &date) const {
  QCalendarWidget::paintCell(painter, rect, date);

  QPen pen = (selected_dates.contains(date)) ? QPen(Qt::red) : QPen(Qt::NoPen);
  painter->setPen(pen);
  painter->setBrush(Qt::transparent);
  painter->drawRect(rect.adjusted(0, 0, -1, -1));
}

Calendar::Calendar(QWidget *parent) : QDialog(parent), ui(new Ui::Calendar) {
  ui->setupUi(this);
}

Calendar::Calendar(const QList<QDate> &dates, QWidget *parent)
    : Calendar(parent) {
  ui->calendar_widget->setDates(dates);
}

Calendar::~Calendar() { delete ui; }

QList<QDate> Calendar::getDates() const {
  return ui->calendar_widget->getDates();
}

void Calendar::on_exit_button_clicked() { close(); }

} // namespace summit
