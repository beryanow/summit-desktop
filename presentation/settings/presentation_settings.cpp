#include "presentation_settings.h"

namespace summit {

PresentationSettings &PresentationSettings::getInstance() {
  static PresentationSettings INSTANCE;
  return INSTANCE;
}

PresentationSettings::PresentationSettings()
    : executor(SingleUseCaseExecutor::getInstance()) {}

} // namespace summit
