#ifndef PRESENTATIONSETTINGS_H
#define PRESENTATIONSETTINGS_H

#include "data/images/images_repository.h"
#include "domain/common/single_usecase_executor.h"
#include "domain/common/usecase_executor.h"
#include "domain/images/load_images_usecase.h"

#include <mutex>
#include <typeinfo>
#include <unordered_map>

namespace summit {

class SettingsListener {
public:
  virtual ~SettingsListener() = default;
  virtual void onSettingsChanged() = 0;
};

class UseCaseWrapper : public QObject {
  Q_OBJECT

  std::shared_ptr<LoadImagesUseCase> use_case;

public:
  UseCaseWrapper(QObject *parent)
      : QObject(parent),
        use_case(new LoadImagesUseCase(ImagesRepository::create())) {}

  std::shared_ptr<LoadImagesUseCase> &getUseCase() { return use_case; }
};

class PresentationSettings : public QObject {
  Q_OBJECT

  std::mutex mutex;
  std::shared_ptr<UseCaseExecutor> executor;
  std::unordered_map<const void *, ImagePointer> background_images;

  SettingsListener *listener;

public:
  static PresentationSettings &getInstance();

  void setListener(SettingsListener *listener) {
    std::lock_guard<std::mutex> lock(mutex);
    this->listener = listener;
  }

  template <class ViewType> ImagePointer getImage() {
    std::lock_guard<std::mutex> lock(mutex);
    return background_images[&typeid(ViewType)];
  }

  template <class ViewType> void setImageId(int id) {
    auto use_case_wrapper = new UseCaseWrapper(this);

    connect(use_case_wrapper->getUseCase().get(), &LoadImagesUseCase::finished,
            this, [this, use_case_wrapper, id](const ImagePointer &image) {
              {
                std::lock_guard<std::mutex> lock(mutex);
                background_images[&typeid(ViewType)] = image;
                use_case_wrapper->deleteLater();
              }
              if (listener) {
                listener->onSettingsChanged();
              }
            });

    connect(use_case_wrapper->getUseCase().get(), &LoadImagesUseCase::failed,
            this, [use_case_wrapper](const DomainError &) {
              /* Do better later */
              use_case_wrapper->deleteLater();
            });
    executor->execute(use_case_wrapper->getUseCase(), id);
  }

private:
  PresentationSettings();
};

} // namespace summit

#endif // PRESENTATIONSETTINGS_H
