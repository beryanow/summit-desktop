#ifndef IMAGES_CONTRACT_H
#define IMAGES_CONTRACT_H

#include "presentation/common/base_presenter.h"
#include "presentation/common/base_view.h"
#include "presentation/common/presentation_error.h"

#include "model/images/image.h"

namespace summit {

class ImagesContract {
public:
  class Presenter : public IBasePresenter {
  public:
    virtual void sendImage(const ImagePointer &image) = 0;
    virtual void loadImage(int id) = 0;
  };

  class View : public IBaseView {
  public:
    virtual void onImageLoadSuccess(const ImagePointer &image) = 0;
    virtual void onImageSendSuccess(const ImagePointer &image) = 0;

    virtual void onImageActionFailure(const PresentationError &error) = 0;
  };
};

} // namespace summit

#endif // IMAGES_CONTRACT_H
