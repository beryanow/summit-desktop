#include "images_presenter.h"

#include "data/images/images_repository.h"

namespace summit {

ImagesPresenter::ImagesPresenter(
    ImagesContract::View *view,
    const std::shared_ptr<UseCaseExecutor> &executor)
    : view(view), executor(executor),
      load_use_case(new LoadImagesUseCase(ImagesRepository::create())),
      send_use_case(new SendImageUseCase(ImagesRepository::create())) {

  connectView();
  connectUseCase();
}

void ImagesPresenter::loadImage(int id) {
  executor->execute(load_use_case, id);
}

void ImagesPresenter::sendImage(const ImagePointer &image) {
  executor->execute(send_use_case, image);
}

void ImagesPresenter::connectView() {}

void ImagesPresenter::connectUseCase() {
  /* Load */
  connect(
      load_use_case.get(), &LoadImagesUseCase::finished, this,
      [this](const ImagePointer &image) { view->onImageLoadSuccess(image); });

  connect(load_use_case.get(), &LoadImagesUseCase::failed, this,
          [this](const DomainError &error) {
            view->onImageActionFailure(PresentationError{});
          });

  /* Send */
  connect(
      send_use_case.get(), &SendImageUseCase::finished, this,
      [this](const ImagePointer &image) { view->onImageSendSuccess(image); });

  connect(send_use_case.get(), &SendImageUseCase::failed, this,
          [this](const DomainError &error) {
            view->onImageActionFailure(PresentationError{});
          });
}

} // namespace summit
