#include "images_viewer.h"
#include "images_presenter.h"

#include "domain/common/single_usecase_executor.h"

#include <QLabel>

namespace summit {

ImagesView::ImagesView() { createPresenter(); }

void ImagesView::addImageSubscriber(int id, ImageSubscriber *subscriber) {
  holder.addSubscriber(id, subscriber);
}

void ImagesView::loadImage(int id) { presenter->loadImage(id); }

void ImagesView::loadAllImages() {
  for (auto id : holder.getKeys()) {
    loadImage(id);
  }
}

void ImagesView::sendImage(const ImagePointer &image) {
  presenter->sendImage(image);
}

void ImagesView::onImageLoadSuccess(const ImagePointer &image) {
  holder.setImage(image);
}

void ImagesView::onImageSendSuccess(const ImagePointer &) {}

void ImagesView::onImageActionFailure(const PresentationError &error) {}

void ImagesView::createPresenter() {
  presenter.reset(
      new ImagesPresenter(this, SingleUseCaseExecutor::getInstance()));
}

} // namespace summit
