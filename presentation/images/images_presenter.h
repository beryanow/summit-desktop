#ifndef IMAGEPRESENTER_H
#define IMAGEPRESENTER_H

#include "domain/common/usecase_executor.h"
#include "domain/images/load_images_usecase.h"
#include "domain/images/send_image_usecase.h"
#include "images_contract.h"

namespace summit {

class ImagesPresenter : public QObject, public ImagesContract::Presenter {
  Q_OBJECT
public:
  ImagesPresenter(ImagesContract::View *view,
                  const std::shared_ptr<UseCaseExecutor> &executor);
  void loadImage(int id) override;
  void sendImage(const ImagePointer &image) override;

private:
  void connectView();
  void connectUseCase();

  ImagesContract::View *view;

  std::shared_ptr<UseCaseExecutor> executor;
  std::shared_ptr<LoadImagesUseCase> load_use_case;
  std::shared_ptr<SendImageUseCase> send_use_case;
};

} // namespace summit

#endif // IMAGEPRESENTER_H
