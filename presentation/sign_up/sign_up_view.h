#ifndef SIGN_UP_VIEW_H
#define SIGN_UP_VIEW_H

#include <QDialog>

#include "model/images/image.h"
#include "presentation/common/view_manager.h"
#include "sign_up_contract.h"

namespace Ui {
class SignUpView;
}

namespace summit {

class SignUpView : public QDialog, public SignUpContract::View {
  Q_OBJECT

public:
  explicit SignUpView(ViewManager *view_manager);
  ~SignUpView();

  void setBackgroundImage(const ImagePointer &background);

public slots:
  void signUpSuccess() override;
  void signUpFailure(const PresentationError &) override;

private slots:
  void on_sign_up_button_clicked();
  void on_cancel_button_clicked();

private:
  void paintEvent(QPaintEvent *event) override;
  void createPresenter() override;
  void blockingAction(bool block);

  ImagePointer background;
  SignUpContract::Presenter *presenter;
  ViewManager *view_manager;
  Ui::SignUpView *ui;
};

} // namespace summit

#endif // SIGN_UP_VIEW_H
