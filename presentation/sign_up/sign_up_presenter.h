#ifndef SIGN_UP_PRESENTER_H
#define SIGN_UP_PRESENTER_H

#include "domain/auth/sign_up_usecase.h"
#include "domain/common/usecase_executor.h"
#include "sign_up_contract.h"

#include <QObject>

namespace summit {

class SignUpPresenter : public QObject, public SignUpContract::Presenter {
  Q_OBJECT
public:
  SignUpPresenter(SignUpContract::View *view,
                  const std::shared_ptr<UseCaseExecutor> &executor);

  void signUp(const RegisterData &register_data) override;

signals:
  void signUpSuccessTrigger();
  void signUpFailureTrigger(const PresentationError &);

private:
  void connectView();
  void connectUseCase();

  SignUpContract::View *view;
  std::shared_ptr<UseCaseExecutor> executor;
  std::shared_ptr<SignUpUseCase> use_case;
};

} // namespace summit
#endif // SIGN_UP_PRESENTER_H
