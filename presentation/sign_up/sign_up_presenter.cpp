#include "sign_up_presenter.h"

#include "data/auth/remote_auth_service.h"

namespace summit {

SignUpPresenter::SignUpPresenter(
    SignUpContract::View *view,
    const std::shared_ptr<UseCaseExecutor> &executor)
    : view(view), executor(executor),
      use_case(new SignUpUseCase(RemoteAuthService::getInstance())) {

  connectView();
  connectUseCase();
}

void SignUpPresenter::connectView() {
  /* Connect view */
  QObject *view_obj = dynamic_cast<QObject *>(view);
  connect(this, SIGNAL(signUpSuccessTrigger()), view_obj,
          SLOT(signUpSuccess()));
  connect(this, SIGNAL(signUpFailureTrigger(const PresentationError &)),
          view_obj, SLOT(signUpFailure(const PresentationError &)));
  setParent(view_obj);
}

void SignUpPresenter::connectUseCase() {
  /* Connect use case */
  connect(use_case.get(), &SignUpUseCase::finished, this,
          [this]() { emit signUpSuccessTrigger(); });

  connect(use_case.get(), &SignUpUseCase::failed, this,
          [this](const DomainError &error) {
            emit signUpFailureTrigger(PresentationError{error.getMessage()});
          });
}

void SignUpPresenter::signUp(const RegisterData &register_data) {
  executor->execute(use_case, register_data);
}

} // namespace summit
