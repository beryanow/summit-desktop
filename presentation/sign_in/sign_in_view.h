#ifndef SIGN_IN_VIEW_H
#define SIGN_IN_VIEW_H

#include "model/images/image.h"
#include "presentation/common/view_manager.h"
#include "sign_in_contract.h"

#include <QDialog>

namespace Ui {
class SignInView;
}

namespace summit {

class SignInView : public QDialog, public SignInContract::View {
  Q_OBJECT

public:
  explicit SignInView(ViewManager *view_manager);
  ~SignInView();

  void setBackgroundImage(const ImagePointer &background);

private slots:
  void on_sign_in_button_clicked();

  void signInSuccess() override;
  void signInFailure(const PresentationError &error) override;

  void on_pushButton_clicked();

protected:
  void paintEvent(QPaintEvent *event) override;

private:
  void createPresenter() override;
  void blockingAction(bool block);

  ImagePointer background;
  SignInContract::Presenter *presenter;
  ViewManager *view_manager;

  Ui::SignInView *ui;
};

} // namespace summit

#endif // SIGN_IN_VIEW_H
