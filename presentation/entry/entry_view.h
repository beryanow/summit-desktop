#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "entry_contract.h"
#include "presentation/common/base_view.h"
#include "presentation/common/view_manager.h"

namespace Ui {
class EntryView;
}

namespace summit {

class EntryView : public QMainWindow, public EntryContract::View {
  Q_OBJECT

public:
  EntryView(QWidget *parent = nullptr);
  ~EntryView();

public slots:
  void autohorizeCompleted() override;
  void authorizeRejected() override;

private:
  void createPresenter() override;

  EntryContract::Presenter *presenter;
  ViewManager *screen_manager;
  Ui::EntryView *ui;
};

} // namespace summit
#endif // MAINWINDOW_H
