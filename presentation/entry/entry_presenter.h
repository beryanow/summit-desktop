#ifndef ENTRYPRESENTER_H
#define ENTRYPRESENTER_H

#include "domain/common/usecase_executor.h"
#include "domain/entry/load_token_usecase.h"
#include "entry_contract.h"

namespace summit {

class EntryPresenter : public QObject, public EntryContract::Presenter {
  Q_OBJECT
public:
  EntryPresenter(EntryContract::View *view,
                 const std::shared_ptr<UseCaseExecutor> &executor,
                 const std::shared_ptr<LoadTokenUseCase> &use_case);

  void tryAuthorize() override;

signals:
  void autohorizeCompletedTrigger();
  void authorizeRejectedTrigger();

private:
  void connectView();
  void connectUseCase();

  EntryContract::View *view;
  std::shared_ptr<UseCaseExecutor> executor;
  std::shared_ptr<LoadTokenUseCase> use_case;
};

} // namespace summit

#endif // ENTRYPRESENTER_H
