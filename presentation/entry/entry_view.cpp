#include "entry_view.h"
#include "ui_entry_view.h"

#include "presentation/common/font/font.h"
#include "presentation/menu/menu_view.h"
#include "presentation/start/start_view.h"

#include "domain/common/single_usecase_executor.h"
#include "domain/entry/load_token_usecase.h"
#include "presentation/entry/entry_presenter.h"

#include <QDebug>
#include <utilities/common/network/request.h>

namespace summit {

EntryView::EntryView(QWidget *parent)
    : QMainWindow(parent), screen_manager(new ViewManager(this)),
      ui(new Ui::EntryView) {
  ui->setupUi(this);

  loadFonts();

  setCentralWidget(new QWidget);
  centralWidget()->setLayout(screen_manager->getLayout());

  /* Create presenter */
  createPresenter();
}

void EntryView::autohorizeCompleted() {
  screen_manager->nextView(new MenuView(screen_manager));
}

void EntryView::authorizeRejected() {
  screen_manager->nextView(new StartView(screen_manager));
}

void EntryView::createPresenter() {
  presenter = new EntryPresenter(
      this, SingleUseCaseExecutor::getInstance(),
      std::shared_ptr<LoadTokenUseCase>(new LoadTokenUseCase));
  presenter->tryAuthorize();
}

EntryView::~EntryView() { delete ui; }

} // namespace summit
