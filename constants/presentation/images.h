#ifndef IMAGES_H
#define IMAGES_H

#include "utilities/common/nonstd/optional.h"
#include <QSize>

extern const nonstd::optional<QSize> IMAGE_CROP_SIZE;

#endif // IMAGES_H
