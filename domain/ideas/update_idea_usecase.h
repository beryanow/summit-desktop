#ifndef UPDATE_IDEA_USECASE_H
#define UPDATE_IDEA_USECASE_H

#include "data/ideas/ideas_data_source.h"
#include "domain/common/usecase.h"
#include "model/ideas/idea.h"

namespace summit {

class UpdateIdeaUseCase : public QObject, public UseCase<Idea, void> {
  Q_OBJECT
public:
  using RequestType = Idea;
  using ResponseType = void;

  explicit UpdateIdeaUseCase(
      const std::shared_ptr<IdeasDataSource> &ideas_repository);

signals:
  void finished() override;
  void failed(const DomainError &error) override;

private:
  void execute(const Idea &idea) override;

  std::shared_ptr<IdeasDataSource> ideas_repository;
};

} // namespace summit

#endif // UPDATE_IDEA_USECASE_H
