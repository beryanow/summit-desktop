#include "update_idea_usecase.h"

namespace summit {

UpdateIdeaUseCase::UpdateIdeaUseCase(
    const std::shared_ptr<IdeasDataSource> &ideas_repository)
    : ideas_repository(std::move(ideas_repository)) {

  connect(ideas_repository.get(), &IdeasDataSource::updateSuccess, this,
          [this]() { emit finished(); });
  connect(ideas_repository.get(), &IdeasDataSource::updateFailure, this,
          [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void UpdateIdeaUseCase::execute(const Idea &idea) {
  ideas_repository->updateIdea(idea);
}

} // namespace summit
