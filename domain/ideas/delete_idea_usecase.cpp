#include "delete_idea_usecase.h"

namespace summit {

DeleteIdeaUseCase::DeleteIdeaUseCase(
    const std::shared_ptr<IdeasDataSource> &ideas_repository)
    : ideas_repository(ideas_repository) {

  connect(ideas_repository.get(), &IdeasDataSource::deleteSuccess, this,
          [this]() { emit finished(); });
  connect(ideas_repository.get(), &IdeasDataSource::deleteFailure, this,
          [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void DeleteIdeaUseCase::execute(const int &id) {
  ideas_repository->deleteIdea(id);
}

} // namespace summit
