#include "load_profile_usecase.h"

namespace summit {

LoadProfileUseCase::LoadProfileUseCase(
    const std::shared_ptr<ProfileDataSource> &profile_repository)
    : profile_repository(profile_repository) {

  connect(
      profile_repository.get(), &ProfileDataSource::loadSuccess, this,
      [this](const UserProfilePointer &profile) { emit finished(profile); });
  connect(profile_repository.get(), &ProfileDataSource::loadFailure, this,
          [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void LoadProfileUseCase::execute() { profile_repository->loadProfile(); }

} // namespace summit
