#ifndef UPDATE_PROFILE_USECASE_H
#define UPDATE_PROFILE_USECASE_H

#include "data/profile/profile_data_source.h"
#include "domain/common/usecase.h"

namespace summit {

class UpdateProfileUseCase : public QObject,
                             public UseCase<UserProfilePointer, void> {
  Q_OBJECT
public:
  using RequestType = UserProfilePointer;
  using ResponseType = void;

  explicit UpdateProfileUseCase(
      const std::shared_ptr<ProfileDataSource> &profile_repository);

signals:
  void finished() override;
  void failed(const DomainError &error) override;

private:
  void execute(const UserProfilePointer &profile) override;

  std::shared_ptr<ProfileDataSource> profile_repository;
};

} // namespace summit

#endif // UPDATE_PROFILE_USECASE_H
