#include "load_progress_usecase.h"

namespace summit {

LoadProgressUseCase::LoadProgressUseCase(
    const std::shared_ptr<ProgressDataSource> &progress_repository)
    : progress_repository(progress_repository) {

  connect(
      progress_repository.get(), &ProgressDataSource::loadSuccess, this,
      [this](const UserProgressList &progress) { emit finished(progress); });
  connect(progress_repository.get(), &ProgressDataSource::loadFailure, this,
          [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void LoadProgressUseCase::execute(const QDate &date) {
  progress_repository->loadProgress(date);
}

} // namespace summit
