#ifndef LOADPROFILEUSECASE_H
#define LOADPROFILEUSECASE_H

#include "data/profile/profile_data_source.h"
#include "domain/common/usecase.h"

namespace summit {

class LoadProfileUseCase : public QObject,
                           public UseCase<void, UserProfilePointer> {
  Q_OBJECT
public:
  using RequestType = void;
  using ResponseType = UserProfilePointer;

  explicit LoadProfileUseCase(
      const std::shared_ptr<ProfileDataSource> &profile_repository);

signals:
  void finished(const UserProfilePointer &profile) override;
  void failed(const DomainError &error) override;

private:
  void execute() override;

  std::shared_ptr<ProfileDataSource> profile_repository;
};

} // namespace summit

#endif // LOADPROFILEUSECASE_H
