#ifndef LOADIMAGESUSECASE_H
#define LOADIMAGESUSECASE_H

#include "data/images/images_data_source.h"
#include "domain/common/usecase.h"
#include "model/images/image.h"

namespace summit {

class LoadImagesUseCase : public QObject, public UseCase<int, ImagePointer> {
  Q_OBJECT
public:
  explicit LoadImagesUseCase(
      const std::shared_ptr<ImagesDataSource> &images_repository);

signals:
  void finished(const ImagePointer &responce) override;
  void failed(const DomainError &error) override;

private:
  void execute(const int &id) override;

  std::shared_ptr<ImagesDataSource> images_repository;
};

} // namespace summit

#endif // LOADIMAGESUSECASE_H
