#include "send_image_usecase.h"

namespace summit {

SendImageUseCase::SendImageUseCase(
    const std::shared_ptr<ImagesDataSource> &images_repository)
    : images_repository(images_repository) {

  connect(images_repository.get(), &ImagesDataSource::sendSuccess, this,
          [this](const ImagePointer &image) { emit finished(image); });
  connect(images_repository.get(), &ImagesDataSource::sendFailure, this,
          [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void SendImageUseCase::execute(const ImagePointer &image) {
  images_repository->sendImage(image);
}

} // namespace summit
