#include "load_token_usecase.h"

#include <chrono>
#include <future>
#include <thread>

#include <QTimer>

Q_DECLARE_METATYPE(summit::DomainError)

namespace summit {

void LoadTokenUseCase::execute() {
  /* Slow load simulation */
  QTimer *timer = new QTimer(this);
  timer->setInterval(500);
  timer->setSingleShot(true);
  connect(timer, &QTimer::timeout, this, [this]() { emit failed({}); });
  timer->start();
}

LoadTokenUseCase::LoadTokenUseCase() {}

} // namespace summit
