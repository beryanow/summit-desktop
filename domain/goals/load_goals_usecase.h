#ifndef LOADGOALSUSECASE_H
#define LOADGOALSUSECASE_H

#include "data/goals/goals_data_source.h"
#include "domain/common/usecase.h"
#include "model/goals/goal.h"

#include <list>

namespace summit {

class LoadGoalsUseCase : public QObject, public UseCase<void, GoalsList> {
  Q_OBJECT
public:
  using RequestType = void;
  using ResponseType = GoalsList;

  explicit LoadGoalsUseCase(
      const std::shared_ptr<GoalsDataSource> &goals_repository);

signals:
  void finished(const GoalsList &responce) override;
  void failed(const DomainError &error) override;

private:
  void execute() override;

  std::shared_ptr<GoalsDataSource> goals_repository;
};

} // namespace summit

#endif // LOADGOALSUSECASE_H
