#include "load_goals_usecase.h"

namespace summit {

LoadGoalsUseCase::LoadGoalsUseCase(
    const std::shared_ptr<GoalsDataSource> &goals_repository)
    : goals_repository(std::move(goals_repository)) {

  connect(goals_repository.get(), &GoalsDataSource::loadSuccess, this,
          [this](const GoalsList &goals) { emit finished(goals); });
  connect(goals_repository.get(), &GoalsDataSource::loadFailure, this,
          [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void LoadGoalsUseCase::execute() { goals_repository->loadGoals(); }

} // namespace summit
