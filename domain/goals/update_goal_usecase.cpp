#include "update_goal_usecase.h"

namespace summit {

summit::UpdateGoalUseCase::UpdateGoalUseCase(
    const std::shared_ptr<GoalsDataSource> &goals_repository)
    : goals_repository(goals_repository) {

  connect(goals_repository.get(), &GoalsDataSource::updateSuccess, this,
          [this]() { emit finished(); });
  connect(goals_repository.get(), &GoalsDataSource::updateFailure, this,
          [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void summit::UpdateGoalUseCase::execute(const Goal &goal) {
  goals_repository->updateGoal(goal);
}

} // namespace summit
