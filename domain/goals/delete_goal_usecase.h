#ifndef DELETE_GOAL_USECASE_H
#define DELETE_GOAL_USECASE_H

#include "data/goals/goals_data_source.h"
#include "domain/common/usecase.h"
#include "model/goals/goal.h"

namespace summit {

class DeleteGoalUseCase : public QObject, public UseCase<int, void> {
  Q_OBJECT
public:
  using RequestType = void;
  using ResponseType = GoalsList;

  explicit DeleteGoalUseCase(
      const std::shared_ptr<GoalsDataSource> &goals_repository);

signals:
  void finished() override;
  void failed(const DomainError &error) override;

private:
  void execute(const int &id) override;

  std::shared_ptr<GoalsDataSource> goals_repository;
};

} // namespace summit

#endif // DELETE_GOAL_USECASE_H
