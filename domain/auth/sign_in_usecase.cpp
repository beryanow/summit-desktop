#include "sign_in_usecase.h"

namespace summit {

SignInUseCase::SignInUseCase(const std::shared_ptr<AuthService> &service)
    : service(service) {

  connect(service.get(), &AuthService::signInSuccess, this,
          [this](const SecurityManager::AuthorizationToken &token) {
            emit finished(token);
          });
  connect(service.get(), &AuthService::signInFailure, this,
          [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void SignInUseCase::execute(const AuthenticationData &request_parameters) {
  service->signIn(request_parameters);
}

} // namespace summit
