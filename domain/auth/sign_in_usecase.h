#ifndef SIGNINUSECASE_H
#define SIGNINUSECASE_H

#include "data/auth/auth_service.h"
#include "domain/common/usecase.h"

namespace summit {

class SignInUseCase
    : public QObject,
      public UseCase<AuthenticationData, SecurityManager::AuthorizationToken> {

  Q_OBJECT
public:
  using RequestType = AuthenticationData;
  using ResponseType = SecurityManager::AuthorizationToken;

  SignInUseCase(const std::shared_ptr<AuthService> &service);

private:
  std::shared_ptr<AuthService> service;

  void execute(const AuthenticationData &request_parameters) override;

signals:
  void finished(const SecurityManager::AuthorizationToken &) override;
  void failed(const DomainError &) override;
};

} // namespace summit

#endif // SIGNINUSECASE_H
