#include "quote_transition_usecase.h"

static constexpr qreal OPACITY_MAX = 1.0;
static constexpr qreal OPACITY_MIN = 0.0;

#include <QDebug>

summit::QuoteTransitionUseCase::QuoteTransitionUseCase(const QuotePointer &from,
                                                       const QuotePointer &to)
    : from(from), to(to), thread(new QThread(this)), shutdown(false) {}

summit::QuoteTransitionUseCase::~QuoteTransitionUseCase() {
  terminate();
  thread->terminate();
  thread->wait();
  thread->deleteLater();
}

void summit::QuoteTransitionUseCase::execute() {
  connect(thread, &QThread::started, this, [this]() {
    auto action = [this](double opacity) {
      emit requestUpdate(opacity);

      /* Sleep */
      QThread::msleep(TRANSITON_PAUSE);
    };

    for (qreal opacity = OPACITY_MAX; !shutdown && opacity > OPACITY_MIN;
         opacity -= TRANSITON_STEP) {
      action(opacity);
    }
    action(OPACITY_MIN);
    emit finished();
  });
  connect(this, &QuoteTransitionUseCase::finished, thread, &QThread::quit);
  moveToThread(thread);
  thread->start();
}

void summit::QuoteTransitionUseCase::terminate() { shutdown = true; }
