#ifndef QUOTETRANSITIONUSECASE_H
#define QUOTETRANSITIONUSECASE_H

#include "domain/common/usecase.h"
#include "model/start/quote.h"

#include <QThread>

namespace summit {

class QuoteTransitionUseCase : public QObject, public UseCase<void, void> {
  Q_OBJECT

  static constexpr qreal TRANSITON_STEP = 0.025;
  static constexpr ulong TRANSITON_PAUSE = 25; // ms

  using QuotePointer = QSharedPointer<const Quote>;

public:
  const QuotePointer from;
  const QuotePointer to;

  QThread *thread;
  std::atomic_bool shutdown;

public:
  QuoteTransitionUseCase(const QuotePointer &from, const QuotePointer &to);
  ~QuoteTransitionUseCase();

  const QuotePointer &getFrom() { return from; }
  const QuotePointer &getTo() { return to; }

  void terminate();
  bool isValid() { return !shutdown; };

private:
  void execute() override;

signals:
  void finished() override;
  void failed(DomainError) override;

  void requestUpdate(qreal opacity);
};

} // namespace summit

#endif // QUOTETRANSITIONUSECASE_H
