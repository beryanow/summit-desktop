#ifndef UPDATE_TRAINING_USECASE_H
#define UPDATE_TRAINING_USECASE_H

#include "data/trainings/trainings_data_source.h"
#include "domain/common/usecase.h"
#include "model/trainings/training_type.h"

namespace summit {

class UpdateTrainingUseCase : public QObject, public UseCase<Training, void> {
  Q_OBJECT
public:
  using RequestType = void;
  using ResponceType = TrainingList;

  explicit UpdateTrainingUseCase(
      std::unique_ptr<TrainingsDataSource> training_repository);

signals:
  void finished() override;
  void failed(const DomainError &) override;

private:
  void execute(const Training &training) override;

  std::unique_ptr<TrainingsDataSource> training_repository;
};

} // namespace summit

#endif // UPDATE_TRAINING_USECASE_H
