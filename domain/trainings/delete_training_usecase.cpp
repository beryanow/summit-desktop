#include "delete_training_usecase.h"

namespace summit {

DeleteTrainingUseCase::DeleteTrainingUseCase(
    std::unique_ptr<TrainingsDataSource> training_repository)
    : training_repository(std::move(training_repository)) {

  connect(this->training_repository.get(),
          &TrainingsDataSource::onTrainingDeleteSuccess, this,
          [this]() { emit finished(); });

  connect(this->training_repository.get(), &TrainingsDataSource::onFailure,
          this, [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void DeleteTrainingUseCase::execute(const int &id) {
  training_repository->deleteTraining(id);
}

} // namespace summit
