#ifndef LOADTRAININGLISTUSECASE_H
#define LOADTRAININGLISTUSECASE_H

#include "data/trainings/trainings_data_source.h"
#include "domain/common/usecase.h"

namespace summit {

class LoadTrainingListUseCase : public QObject,
                                public UseCase<void, TrainingList> {
  Q_OBJECT
public:
  using RequestType = void;
  using ResponceType = TrainingList;

  explicit LoadTrainingListUseCase(
      std::unique_ptr<TrainingsDataSource> training_repository);

signals:
  void finished(const TrainingList &) override;
  void failed(const DomainError &) override;

private:
  void execute() override;

  std::unique_ptr<TrainingsDataSource> training_repository;
};

} // namespace summit
#endif // LOADTRAININGLISTUSECASE_H
