#include "load_training_list_usecase.h"

namespace summit {

LoadTrainingListUseCase::LoadTrainingListUseCase(
    std::unique_ptr<TrainingsDataSource> training_repository)
    : training_repository(std::move(training_repository)) {

  connect(this->training_repository.get(),
          &TrainingsDataSource::onTrainingListLoadSuccess, this,
          [this](const TrainingList &trainings) { emit finished(trainings); });

  connect(this->training_repository.get(), &TrainingsDataSource::onFailure,
          this, [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void LoadTrainingListUseCase::execute() {
  training_repository->loadAllTrainings();
}

} // namespace summit
