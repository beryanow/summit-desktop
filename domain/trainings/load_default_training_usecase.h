#ifndef LOAD_DEFAULT_TRAINING_H
#define LOAD_DEFAULT_TRAINING_H

#include "data/trainings/trainings_data_source.h"
#include "domain/common/usecase.h"
#include "model/trainings/training_type.h"

namespace summit {

class LoadDefaultTrainingUseCase : public QObject,
                                   public UseCase<TrainingType, Training> {
  Q_OBJECT
public:
  using RequestType = void;
  using ResponceType = TrainingList;

  explicit LoadDefaultTrainingUseCase(
      std::unique_ptr<TrainingsDataSource> training_repository);

signals:
  void finished(const Training &) override;
  void failed(const DomainError &) override;

private:
  void execute(const TrainingType &type) override;

  std::unique_ptr<TrainingsDataSource> training_repository;
};

} // namespace summit

#endif // LOAD_DEFAULT_TRAINING_H
