#include "update_training_usecase.h"

namespace summit {

UpdateTrainingUseCase::UpdateTrainingUseCase(
    std::unique_ptr<TrainingsDataSource> training_repository)
    : training_repository(std::move(training_repository)) {

  connect(this->training_repository.get(),
          &TrainingsDataSource::onTrainingUpdateSuccess, this,
          [this]() { emit finished(); });

  connect(this->training_repository.get(), &TrainingsDataSource::onFailure,
          this, [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void UpdateTrainingUseCase::execute(const Training &trainig) {
  training_repository->updateTraining(trainig);
}

} // namespace summit
