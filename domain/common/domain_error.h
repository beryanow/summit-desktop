#ifndef DOMAINERROR_H
#define DOMAINERROR_H

#include <QString>

namespace summit {

class DomainError {
  QString message;

public:
  DomainError();

  DomainError(const QString &message);

  const QString &getMessage() const { return message; }
};

} // namespace summit
#endif // DOMAINERROR_H
