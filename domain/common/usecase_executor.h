#ifndef USECASEEXECUTOR_H
#define USECASEEXECUTOR_H

#include "usecase.h"

namespace summit {

class UseCaseExecutor {
public:
  template <class UseCaseType, class RequestParametersType>
  void execute(std::shared_ptr<UseCaseType> &use_case,
               RequestParametersType &&request_parameters) {
    use_case->setRequestParameters(
        std::forward<RequestParametersType>(request_parameters));
    use_case->run();
  }

  template <class UseCaseType>
  void execute(std::shared_ptr<UseCaseType> &use_case) {
    use_case->run();
  }

  virtual ~UseCaseExecutor() = default;
};

} // namespace summit

#endif // USECASEEXECUTOR_H
