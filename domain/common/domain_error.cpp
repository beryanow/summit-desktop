#include "domain_error.h"

namespace summit {

DomainError::DomainError() {}

DomainError::DomainError(const QString &message) : message(message) {}

} // namespace summit
