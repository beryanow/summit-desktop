#include "delete_recipe_usecase.h"

namespace summit {

DeleteRecipeUseCase::DeleteRecipeUseCase(
    const std::shared_ptr<NutritionDataSource> &recipes_repository)
    : recipes_repository(recipes_repository) {

  connect(recipes_repository.get(), &NutritionDataSource::deleteSuccess, this,
          [this]() { emit finished(); });
  connect(recipes_repository.get(), &NutritionDataSource::deleteFailure, this,
          [this](const DataError &error) {
            emit failed(DomainError{error.getMessage()});
          });
}

void DeleteRecipeUseCase::execute(const int &id) {
  recipes_repository->deleteRecipe(id);
}

} // namespace summit
