QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++14

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0



# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH += $$PWD



RESOURCES += \
    resource.qrc \
    sportsmen.qrc

DISTFILES +=

FORMS += \
    presentation/actions/action_view.ui \
    presentation/actions/goals/goals_editor.ui \
    presentation/actions/nutrition/recipe_editor.ui \
    presentation/actions/profile/profile_editor.ui \
    presentation/actions/trainings/training_editor.ui \
    presentation/boards/board_view.ui \
    presentation/common/calendar/calendar.ui \
    presentation/entry/entry_view.ui \
    presentation/menu/menu_view.ui \
    presentation/sign_in/sign_in_view.ui \
    presentation/sign_up/sign_up_view.ui \
    presentation/start/start_view.ui

HEADERS += \
    constants/common/strings.h \
    constants/network/network_constants.h \
    constants/presentation/images.h \
    constants/presentation/styles.h \
    constants/presentation/widget_constants.h \
    data/common/data_error.h \
    data/common/source/remote/default_data_deleter.h \
    data/common/source/remote/default_data_loader.h \
    data/common/source/remote/default_data_updater.h \
    data/common/source/remote/setup.h \
    data/goals/goals_data_source.h \
    data/goals/goals_repository.h \
    data/ideas/ideas_data_source.h \
    data/ideas/ideas_repository.h \
    data/images/images_data_source.h \
    data/images/images_repository.h \
    data/nutrition/nutrition_data_source.h \
    data/nutrition/nutrition_repository.h \
    data/profile/profile_data_source.h \
    data/profile/profile_repository.h \
    data/profile/progress_data_source.h \
    data/profile/progress_repository.h \
    data/auth/auth_service.h \
    data/auth/remote_auth_service.h \
    data/trainings/trainings_data_source.h \
    data/trainings/trainings_repository.h \
    domain/common/domain_error.h \
    domain/common/single_usecase_executor.h \
    domain/common/usecase.h \
    domain/common/usecase_executor.h \
    domain/entry/load_token_usecase.h \
    domain/goals/delete_goal_usecase.h \
    domain/goals/load_goals_usecase.h \
    domain/goals/update_goal_usecase.h \
    domain/ideas/delete_idea_usecase.h \
    domain/ideas/load_ideas_usecase.h \
    domain/ideas/update_idea_usecase.h \
    domain/images/load_images_usecase.h \
    domain/images/send_image_usecase.h \
    domain/nutrition/delete_recipe_usecase.h \
    domain/nutrition/load_recipes_usecase.h \
    domain/nutrition/update_recipe_usecase.h \
    domain/profile/load_profile_usecase.h \
    domain/profile/load_progress_usecase.h \
    domain/auth/sign_in_usecase.h \
    domain/auth/sign_up_usecase.h \
    domain/profile/update_profile_usecase.h \
    domain/start/quote_transition_usecase.h \
    domain/trainings/delete_training_usecase.h \
    domain/trainings/load_default_training_usecase.h \
    domain/trainings/load_training_list_usecase.h \
    domain/trainings/update_training_usecase.h \
    model/auth/authentication_data.h \
    model/auth/register_data.h \
    model/exercises/exercise.h \
    model/goals/goal.h \
    model/ideas/idea.h \
    model/images/image.h \
    model/nutrition/recipe.h \
    model/profile/user_profile.h \
    model/profile/user_progress.h \
    model/start/quote.h \
    model/tasks/task.h \
    model/trainings/training.h \
    model/trainings/training_type.h \
    presentation/actions/action_presenter.h \
    presentation/actions/action_view.h \
    presentation/actions/goals/goals_edit_view.h \
    presentation/actions/goals/goals_editor.h \
    presentation/actions/goals/task_action_factory.h \
    presentation/actions/goals/task_actions.h \
    presentation/actions/ideas/ideas_edit_view.h \
    presentation/actions/nutrition/nutrition_edit_view.h \
    presentation/actions/nutrition/recipe_editor.h \
    presentation/actions/profile/profile_edit_view.h \
    presentation/actions/profile/profile_editor.h \
    presentation/actions/trainings/training_edit_view.h \
    presentation/actions/trainings/training_editor.h \
    presentation/boards/board_presenter.h \
    presentation/boards/board_view.h \
    presentation/boards/goals/goal_widget_view_adapter.h \
    presentation/boards/goals/goals_view.h \
    presentation/boards/ideas/idea_widget_view_adapter.h \
    presentation/boards/ideas/ideas_view.h \
    presentation/boards/nutrition/nutrition_view.h \
    presentation/boards/nutrition/recipe_widget_view_adapter.h \
    presentation/boards/profile/profile_view.h \
    presentation/boards/profile/profile_widget_view_adapter.h \
    presentation/boards/profile/progress_widget_view_adapter.h \
    presentation/boards/trainings/training_list_view.h \
    presentation/boards/trainings/training_list_widget_view_adapter.h \
    presentation/boards/trainings/training_widget_view_adapter.h \
    presentation/boards/trainings/trainings_view.h \
    presentation/common/base_presenter.h \
    presentation/common/base_view.h \
    presentation/common/button/rounded_button.h \
    presentation/common/calendar/calendar.h \
    presentation/common/font/font.h \
    presentation/common/images/image_subscriber.h \
    presentation/common/images/image_subscribers_holder.h \
    presentation/common/label/elided_label.h \
    presentation/common/label/filling_label.h \
    presentation/common/label/transparent_label.h \
    presentation/common/presentation_error.h \
    presentation/common/utils/size_fixer.h \
    presentation/common/view_manager.h \
    presentation/common/widget/scrollable_widgets_viewer.h \
    presentation/common/widget/widget.h \
    presentation/common/widget/widget_item.h \
    presentation/common/widget/widget_utilities.h \
    presentation/common/widget/widgets_viewer.h \
    presentation/common/widget/widgets_viewer_adapter.h \
    presentation/entry/entry_contract.h \
    presentation/entry/entry_presenter.h \
    presentation/entry/entry_view.h \
    presentation/images/images_contract.h \
    presentation/images/images_presenter.h \
    presentation/images/images_viewer.h \
    presentation/menu/menu_view.h \
    presentation/settings/presentation_settings.h \
    presentation/sign_in/sign_in_contract.h \
    presentation/sign_in/sign_in_presenter.h \
    presentation/sign_in/sign_in_view.h \
    presentation/sign_up/sign_up_contract.h \
    presentation/sign_up/sign_up_presenter.h \
    presentation/sign_up/sign_up_view.h \
    presentation/start/start_contract.h \
    presentation/start/start_presenter.h \
    presentation/start/start_view.h \
    security/security_manager.h \
    utilities/common/advanced/advanced_qobject.h \
    utilities/common/json/common.h \
    utilities/common/json/json_parser.h \
    utilities/common/json/properties.h \
    utilities/common/network/request.h \
    utilities/common/nonstd/cyclic_list.h \
    utilities/common/nonstd/emplace.h \
    utilities/common/nonstd/error.h \
    utilities/common/nonstd/optional.h

SOURCES += \
    constants/network/network_constants.cpp \
    constants/presentation/images.cpp \
    constants/presentation/styles.cpp \
    constants/presentation/widget_constants.cpp \
    data/common/data_error.cpp \
    data/common/source/remote/setup.cpp \
    data/goals/goals_repository.cpp \
    data/ideas/ideas_repository.cpp \
    data/images/images_repository.cpp \
    data/nutrition/nutrition_repository.cpp \
    data/profile/profile_repository.cpp \
    data/profile/progress_repository.cpp \
    data/auth/remote_auth_service.cpp \
    data/trainings/trainings_repository.cpp \
    domain/common/domain_error.cpp \
    domain/common/single_usecase_executor.cpp \
    domain/entry/load_token_usecase.cpp \
    domain/goals/delete_goal_usecase.cpp \
    domain/goals/load_goals_usecase.cpp \
    domain/goals/update_goal_usecase.cpp \
    domain/ideas/delete_idea_usecase.cpp \
    domain/ideas/load_ideas_usecase.cpp \
    domain/ideas/update_idea_usecase.cpp \
    domain/images/load_images_usecase.cpp \
    domain/images/send_image_usecase.cpp \
    domain/nutrition/delete_recipe_usecase.cpp \
    domain/nutrition/load_recipes_usecase.cpp \
    domain/nutrition/update_recipe_usecase.cpp \
    domain/profile/load_profile_usecase.cpp \
    domain/profile/load_progress_usecase.cpp \
    domain/auth/sign_in_usecase.cpp \
    domain/auth/sign_up_usecase.cpp \
    domain/profile/update_profile_usecase.cpp \
    domain/start/quote_transition_usecase.cpp \
    domain/trainings/delete_training_usecase.cpp \
    domain/trainings/load_default_training_usecase.cpp \
    domain/trainings/load_training_list_usecase.cpp \
    domain/trainings/update_training_usecase.cpp \
    main.cpp \
    model/auth/authentication_data.cpp \
    model/auth/register_data.cpp \
    model/exercises/exercise.cpp \
    model/goals/goal.cpp \
    model/ideas/idea.cpp \
    model/images/image.cpp \
    model/nutrition/recipe.cpp \
    model/profile/user_profile.cpp \
    model/profile/user_progress.cpp \
    model/start/quote.cpp \
    model/tasks/task.cpp \
    model/trainings/training.cpp \
    presentation/actions/action_presenter.cpp \
    presentation/actions/action_view.cpp \
    presentation/actions/goals/goals_edit_view.cpp \
    presentation/actions/goals/goals_editor.cpp \
    presentation/actions/goals/task_actions.cpp \
    presentation/actions/ideas/ideas_edit_view.cpp \
    presentation/actions/nutrition/nutrition_edit_view.cpp \
    presentation/actions/nutrition/recipe_editor.cpp \
    presentation/actions/profile/profile_edit_view.cpp \
    presentation/actions/profile/profile_editor.cpp \
    presentation/actions/trainings/training_edit_view.cpp \
    presentation/actions/trainings/training_editor.cpp \
    presentation/boards/board_presenter.cpp \
    presentation/boards/board_view.cpp \
    presentation/boards/goals/goal_widget_view_adapter.cpp \
    presentation/boards/goals/goals_view.cpp \
    presentation/boards/ideas/idea_widget_view_adapter.cpp \
    presentation/boards/ideas/ideas_view.cpp \
    presentation/boards/nutrition/nutrition_view.cpp \
    presentation/boards/nutrition/recipe_widget_view_adapter.cpp \
    presentation/boards/profile/profile_view.cpp \
    presentation/boards/profile/profile_widget_view_adapter.cpp \
    presentation/boards/profile/progress_widget_view_adapter.cpp \
    presentation/boards/trainings/training_list_view.cpp \
    presentation/boards/trainings/training_list_widget_view_adapter.cpp \
    presentation/boards/trainings/training_widget_view_adapter.cpp \
    presentation/boards/trainings/trainings_view.cpp \
    presentation/common/button/rounded_button.cpp \
    presentation/common/calendar/calendar.cpp \
    presentation/common/font/font.cpp \
    presentation/common/images/image_subscribers_holder.cpp \
    presentation/common/label/elided_label.cpp \
    presentation/common/label/filling_label.cpp \
    presentation/common/label/transparent_label.cpp \
    presentation/common/presentation_error.cpp \
    presentation/common/utils/size_fixer.cpp \
    presentation/common/view_manager.cpp \
    presentation/common/widget/scrollable_widgets_viewer.cpp \
    presentation/common/widget/widget.cpp \
    presentation/common/widget/widget_item.cpp \
    presentation/common/widget/widget_utilities.cpp \
    presentation/entry/entry_presenter.cpp \
    presentation/entry/entry_view.cpp \
    presentation/images/images_presenter.cpp \
    presentation/images/images_viewer.cpp \
    presentation/menu/menu_view.cpp \
    presentation/settings/presentation_settings.cpp \
    presentation/sign_in/sign_in_presenter.cpp \
    presentation/sign_in/sign_in_view.cpp \
    presentation/sign_up/sign_up_presenter.cpp \
    presentation/sign_up/sign_up_view.cpp \
    presentation/start/start_presenter.cpp \
    presentation/start/start_view.cpp \
    security/security_manager.cpp \
    utilities/common/advanced/advanced_qobject.cpp \
    utilities/common/network/request.cpp
